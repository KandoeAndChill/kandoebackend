package be.kdg.kandoebackend.schedulers;

import be.kdg.kandoebackend.model.Session;
import be.kdg.kandoebackend.service.SessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
public class ScheduledTasks {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
    private final SessionService sessionService;

    @Autowired
    public ScheduledTasks(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    //Laad sessies tussen nu en binnen 59sec en zet al deze op actief
    @Scheduled(fixedDelay = 60000)
    public void scheduleTaskWithFixedDelay() {
        logger.info("Fixed Delay Task :: Execution Time - {}", dateTimeFormatter.format(LocalDateTime.now()));
        List<Session> sessionList = sessionService.getAllSessionsBetweenNowAndAMinute();
        int counter = 0;
        for(Session session : sessionList){
            sessionService.startSession(session.getId());
            counter++;
        }
        logger.info("Number of sessions switched to active: " + counter);
        counter = 0;
    }
}
