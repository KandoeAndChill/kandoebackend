package be.kdg.kandoebackend.error;

public class MinAmountOfCardsException extends RuntimeException {

    private final String message;

    public MinAmountOfCardsException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
