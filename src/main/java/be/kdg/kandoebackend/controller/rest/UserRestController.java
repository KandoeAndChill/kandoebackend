package be.kdg.kandoebackend.controller.rest;

import be.kdg.kandoebackend.dto.UserResponseDTO;
import be.kdg.kandoebackend.model.User;
import be.kdg.kandoebackend.dto.UserLoginDTO;
import be.kdg.kandoebackend.service.SessionService;
import be.kdg.kandoebackend.service.UserService;
import io.swagger.annotations.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@CrossOrigin(origins = {"http://localhost:4200"}, maxAge = 3000)
@RestController
@RequestMapping("/users")
@Api(tags = "users")
public class UserRestController {
    private final UserService userService;
    private final ModelMapper modelMapper;
    private final SessionService sessionService;

    @Autowired
    public UserRestController(UserService userService, ModelMapper modelMapper, SessionService sessionService) {
        this.userService = userService;
        this.modelMapper = modelMapper;
        this.sessionService = sessionService;
    }

    private boolean isEmail(String userIdentifier) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";
        return userIdentifier.matches(emailRegex);
    }

    @PostMapping("/signin")
    @ApiOperation(value = "${UserController.signin}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 422, message = "Invalid username/password supplied")})
    @CrossOrigin(origins = "http://localhost:4200")
    public String login(@RequestBody UserLoginDTO user) {
        String username;
        if (isEmail(user.getUserIdentifier())) {
            username = userService.findUserByEmail(user.getUserIdentifier()).getUsername();
        } else {
            username = user.getUserIdentifier();
        }
        return userService.signin(username, user.getPassword());
    }

    @PostMapping("/save")
    @ApiOperation(value = "${UserController.save}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 403, message = "Access denied"),
            @ApiResponse(code = 422, message = "Username is already in use"),
            @ApiResponse(code = 500, message = "Expired or invalid JWT token")})
    @CrossOrigin(origins = "http://localhost:4200")
    public String signup(@ApiParam("Signup User") @Valid @RequestBody User user) {
        return userService.save(user);
    }

    @GetMapping(value = "/me")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
    @ApiOperation(value = "${UserController.me}", response = UserResponseDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 403, message = "Access denied"),
            @ApiResponse(code = 500, message = "Expired or invalid JWT token")})
    @CrossOrigin(origins = "http://localhost:4200")
    public UserResponseDTO whoami(HttpServletRequest req) {
        return modelMapper.map(userService.whoami(req), UserResponseDTO.class);
    }

    @PutMapping(value = "/update")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
    @ApiOperation(value = "${UserController.me}", response = UserResponseDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 403, message = "Access denied"),
            @ApiResponse(code = 500, message = "Expired or invalid JWT token")})
    @CrossOrigin(origins = "http://localhost:4200")
    public UserResponseDTO updateUser(HttpServletRequest req, @ApiParam("Update User") @Valid @RequestBody User user) {
        User oldUser = userService.whoami(req);
        return modelMapper.map(userService.updateUser(oldUser.getId(), user), UserResponseDTO.class);
    }

    @GetMapping(value = "/isThemeOrganiser/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
    @ApiOperation(value = "${UserController.me}", response = UserResponseDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 403, message = "Access denied"),
            @ApiResponse(code = 500, message = "Expired or invalid JWT token")})
    @CrossOrigin(origins = "http://localhost:4200")
    public boolean isThemeOrganiser(@PathVariable(value = "id") int themeId, HttpServletRequest req) {
        return userService.isThemeOrganiser(themeId, req);
    }

    @GetMapping(value = "/isSessionOrganiser/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
    @ApiOperation(value = "${UserController.me}", response = UserResponseDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 403, message = "Access denied"),
            @ApiResponse(code = 500, message = "Expired or invalid JWT token")})
    @CrossOrigin(origins = "http://localhost:4200")
    public boolean isOrganiser(@PathVariable(value = "id") int sessionId, HttpServletRequest req) {
        User user = userService.whoami(req);
        return sessionService.isSessionOrganiser(user, sessionId);
    }

    @GetMapping(value = "/findUserNameById/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
    @ApiOperation(value = "${UserController.findUserNameById}", response = UserResponseDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 403, message = "Access denied"),
            @ApiResponse(code = 500, message = "Expired or invalid JWT token")})
    public String findUserNameById(@PathVariable(value = "id") long userId) {
        return userService.getUserById(userId).get().getUsername();
    }
}