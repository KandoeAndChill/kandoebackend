package be.kdg.kandoebackend.controller.rest;

import be.kdg.kandoebackend.dto.CreateRatingDTO;
import be.kdg.kandoebackend.dto.RatingResponseDTO;
import be.kdg.kandoebackend.error.CustomException;
import be.kdg.kandoebackend.model.Card;
import be.kdg.kandoebackend.dto.CardDTO;
import be.kdg.kandoebackend.model.Rating;
import be.kdg.kandoebackend.model.User;
import be.kdg.kandoebackend.service.CardService;
import be.kdg.kandoebackend.service.RatingService;
import be.kdg.kandoebackend.service.UserService;
import io.swagger.annotations.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/cards")
@Api(tags = "cards")
@CrossOrigin(origins = "http://localhost:4200")
public class CardRestController {
    private final CardService cardService;
    private final ModelMapper modelMapper;
    private final UserService userService;
    private final RatingService ratingService;

    @Autowired
    public CardRestController(CardService cardService, ModelMapper modelMapper, UserService userService, RatingService ratingService) {
        this.cardService = cardService;
        this.modelMapper = modelMapper;
        this.userService = userService;
        this.ratingService = ratingService;
    }

    @PostMapping("/save")
    @ApiOperation(value = "${CardController.save}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "The name is either to short or too long! ( minsize : 4, maxsize: 20 )"),
            @ApiResponse(code = 422, message = "Card already exists"),
            @ApiResponse(code = 200, message = "A new card has been created")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<CardDTO> createCard(@ApiParam("Signup ApplicationUser") @Valid @RequestBody CardDTO cardDTO) {

        Card cardIn = modelMapper.map(cardDTO, Card.class);
        Card cardOut = cardService.save(cardIn);
        return new ResponseEntity<>(modelMapper.map(cardOut, CardDTO.class), HttpStatus.CREATED);
    }

    @GetMapping("/find/{id}")
    @ApiOperation(value = "${CardController.find}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A card has been loaded")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<CardDTO> loadCard(@PathVariable(value = "id") Long id) throws CustomException {
        Card card = cardService.load(id);
        return new ResponseEntity<>(modelMapper.map(card, CardDTO.class), HttpStatus.OK);
    }

    @GetMapping("/findAllCards/{id}")
    @ApiOperation(value = "${CardController.findAll}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "All cards have been loaded")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<CardDTO>> loadCardList(@PathVariable(value = "id") Long id) throws CustomException {
        List<CardDTO> cardDTOList = new ArrayList<>();
        List<Card> cardList = cardService.getCardsForTheme(id);
        for (Card card : cardList){
            CardDTO cardDTO = modelMapper.map(card, CardDTO.class);
            cardDTOList.add(cardDTO);
        }
        return new ResponseEntity<>(cardDTOList, HttpStatus.OK);
    }

    @PostMapping("/addCardRating")
    @ApiOperation(value = "${CardController.addCardRating}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A card has been rated")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<RatingResponseDTO>> addCardRating(@Valid @RequestBody CreateRatingDTO createRatingDTO, HttpServletRequest req) throws CustomException {

        long cardId = createRatingDTO.getCardId();
        Rating rating = modelMapper.map(createRatingDTO, Rating.class);
        User user = userService.whoami(req);
        List<Rating> ratingsForCard = cardService.addRating(cardId, rating, user);

        // Complex mapping which cannot be done with ModelMapper....
        List<RatingResponseDTO> ratingResponseDTOS = new ArrayList<RatingResponseDTO>();
        RatingResponseDTO ratingResponseDTO;

        for (Rating cardRating : ratingsForCard) {
            ratingResponseDTO = new RatingResponseDTO();
            ratingResponseDTO.setComment(cardRating.getComment());
            ratingResponseDTO.setUsername(cardRating.getUser().getUsername());
            ratingResponseDTOS.add(ratingResponseDTO);
        }
        return new ResponseEntity<>(ratingResponseDTOS, HttpStatus.OK);
    }

    @GetMapping("/findAllRatingsByCard/{id}")
    @ApiOperation(value = "${CardController.findAllRatingsByCard}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "Returned all ratings for a card")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<RatingResponseDTO>> findAllRatingsByCard(@PathVariable(value = "id") Long cardId) throws CustomException {
        Card card = cardService.load(cardId);
        List<Rating> ratings = this.ratingService.getRatingByCard(card);

        // Complex mapping which cannot be done with ModelMapper....
        List<RatingResponseDTO> ratingResponseDTOS = new ArrayList<RatingResponseDTO>();
        RatingResponseDTO ratingResponseDTO;

        for (Rating cardRating : ratings) {
            ratingResponseDTO = new RatingResponseDTO();
            ratingResponseDTO.setComment(cardRating.getComment());
            ratingResponseDTO.setUsername(cardRating.getUser().getUsername());
            ratingResponseDTOS.add(ratingResponseDTO);
        }
        return new ResponseEntity<>(ratingResponseDTOS, HttpStatus.OK);
    }
}