package be.kdg.kandoebackend.controller.rest;

import be.kdg.kandoebackend.dto.*;
import be.kdg.kandoebackend.error.CustomException;
import be.kdg.kandoebackend.model.Card;
import be.kdg.kandoebackend.model.CardSession;
import be.kdg.kandoebackend.model.Session;
import be.kdg.kandoebackend.model.User;
import be.kdg.kandoebackend.service.*;
import io.swagger.annotations.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/sessions")
@Api(tags = "sessions")
public class SessionRestController {
    private final SessionService sessionService;
    private final UserService userService;
    private final EmailService emailService;
    private final CardSessionService cardSessionService;

    private ModelMapper modelMapper = new ModelMapper();

    @Autowired
    public SessionRestController(SessionService sessionService, UserService userService, EmailService emailService, CardSessionService cardSessionService) {
        this.sessionService = sessionService;
        this.userService = userService;
        this.emailService = emailService;
        this.cardSessionService = cardSessionService;
    }

    @PostMapping("/save")
    @ApiOperation(value = "${SessionController.create}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A new Session has been created")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<SessionDTO> createSession(@ApiParam("Signup ApplicationUser") @Valid @RequestBody SessionDTO sessionDTO, HttpServletRequest req){
        User organiser = userService.whoami(req);
        sessionDTO.setOrganiserId(organiser.getId());
        Session sessionIn = modelMapper.map(sessionDTO, Session.class);
        sessionIn.addToUserList(organiser);
        Session sessionOut = sessionService.createSession(sessionIn);
        SessionDTO sessionDTOout = modelMapper.map(sessionOut, SessionDTO.class);
        return new ResponseEntity<>(sessionDTOout, HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "${SessionController.delete}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A Session has been deleted")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public void deleteSession(@ApiParam("Id") @PathVariable(value = "id") Long id) throws CustomException {
        Session toBeRemoved = sessionService.load(id);
        sessionService.deleteSession(toBeRemoved.getId());
    }

    @PutMapping("/inviteToSession")
    @ApiOperation(value = "${SessionController.invite}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A new user has been invited")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<SessionDTO> inviteToSession(@RequestBody UserInviteDTO userInviteDTO) throws CustomException {
        Session sessionIn = sessionService.load(userInviteDTO.getSessionId());
        for (String mail : userInviteDTO.getMailList()) {
            User user = userService.findUserByEmail(mail);
            if (!sessionService.isSessionParticipant(user, userInviteDTO.getSessionId())) {
                sessionIn.addToUserList(user);
                if (userService.isMailRegistered(mail)) {
                    emailService.sendSimpleMessage(user.getEmail(), "Session Invitation", "Dear " + user.getUsername()
                            + ",\n\nWe would like to inform you that you have been invited by " + sessionIn.getOrganiser().getUsername() +
                            " to join a session about " + sessionIn.getTheme().getName() + ".\nClick the link below to go to the session and participate." +
                            "\n\nTogether, you Kandoe it,\nKandoe&Chill-team");
                } else {
                    emailService.sendSimpleMessage(mail, "Kandoe&Chill Invitation", "Greetings," +
                            ",\n\nYou have been invited by " + sessionIn.getOrganiser().getUsername() +
                            " to join the platform Kandoe&Chill to participate in a session: " + sessionIn.getName() +
                            ".\nPlease register using the link below so you can join this session." +
                            "\nhttps://localhost:4200/RegisterForSession/1" +
                            "\n\nTogether, we Kandoe it,\nKandoe&Chill-team");
                }
            }
        }
        Session session = sessionService.save(sessionIn);
        return new ResponseEntity<>(modelMapper.map(session, SessionDTO.class), HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    @ApiOperation(value = "${SessionController.find}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A session has been loaded")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<SessionDTO> loadSession(@PathVariable(value = "id") Long id) throws CustomException {
        Session session = sessionService.load(id);
        return new ResponseEntity<>(modelMapper.map(session, SessionDTO.class), HttpStatus.OK);
    }

    @PostMapping("/clone/{id}")
    @ApiOperation(value = "${SessionController.clone}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A session has been cloned")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<SessionDTO> cloneSession(@PathVariable(value = "id") Long id, HttpServletRequest req){
        User organiser = userService.whoami(req);
        Session session = sessionService.cloneSession(id, organiser);
        return new ResponseEntity<>(modelMapper.map(session, SessionDTO.class), HttpStatus.CREATED);
    }

    @GetMapping("/findAllSessions/{id}")
    @ApiOperation(value = "${SessionController.findAll}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A list of sessions has been loaded")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<SessionDTO>> loadSessionList(@PathVariable(value = "id") Long id) throws CustomException {
        List<SessionDTO> sessionDTOList = new ArrayList<>();
        List<Session> sessionList = sessionService.getAllSessionsByTheme(id);
        for(Session session : sessionList){
            SessionDTO sessionDTO = modelMapper.map(session, SessionDTO.class);
            sessionDTO.setState(sessionService.checkState(session.getId()));
            sessionDTOList.add(sessionDTO);
        }
        return new ResponseEntity<>(sessionDTOList, HttpStatus.OK);
    }

    @GetMapping("/findAllPassedSessions/{id}")
    @ApiOperation(value = "${SessionController.findAllPast}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A list of passed sessions has been loaded")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<SessionDTO>> loadPassedSessionList(@PathVariable(value = "id") Long id) throws CustomException {
        List<SessionDTO> sessionDTOList = new ArrayList<>();
        List<Session> sessionList = sessionService.getAllPassedSessionsByTheme(id);
        for(Session session : sessionList){
            SessionDTO sessionDTO = modelMapper.map(session, SessionDTO.class);
            sessionDTOList.add(sessionDTO);
        }
        return new ResponseEntity<>(sessionDTOList, HttpStatus.OK);
    }

    @GetMapping("/findAllActiveSessions/{id}")
    @ApiOperation(value = "${SessionController.findAllActive}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A list of active sessions has been loaded")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<SessionDTO>> loadActiveSessionList(@PathVariable(value = "id") Long id) throws CustomException {
        List<SessionDTO> sessionDTOList = new ArrayList<>();
        List<Session> sessionList = sessionService.getAllActiveSessionsByTheme(id);
        for(Session session : sessionList){
            SessionDTO sessionDTO = modelMapper.map(session, SessionDTO.class);
            sessionDTOList.add(sessionDTO);
        }
        return new ResponseEntity<>(sessionDTOList, HttpStatus.OK);
    }

    @GetMapping("/findAllPlannedSessions/{id}")
    @ApiOperation(value = "${SessionController.findAllPlanned}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A list of planned sessions has been loaded")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<SessionDTO>> loadPlannedSessionList(@PathVariable(value = "id") Long id) throws CustomException {
        List<SessionDTO> sessionDTOList = new ArrayList<>();
        List<Session> sessionList = sessionService.getAllPlannedSessionsByTheme(id);
        for(Session session : sessionList){
            SessionDTO sessionDTO = modelMapper.map(session, SessionDTO.class);
            sessionDTOList.add(sessionDTO);
        }
        return new ResponseEntity<>(sessionDTOList, HttpStatus.OK);
    }

    @GetMapping("/findAllSessionsByLoggedInOrganiser")
    @ApiOperation(value = "${SessionController.findAllByOrganiser}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A list of sessions from organiser has been loaded")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<SessionDTO>> loadSessionListByOrganiser(HttpServletRequest request) throws CustomException {
        User organiser = userService.whoami(request);
        List<SessionDTO> sessionDTOList = new ArrayList<>();
        List<Session> sessionList = sessionService.getAllSessionsByOrganiser(organiser.getId());
        for(Session session : sessionList){
            SessionDTO sessionDTO = modelMapper.map(session, SessionDTO.class);
            sessionDTO.setState(sessionService.checkState(session.getId()));
            sessionDTOList.add(sessionDTO);
        }
        return new ResponseEntity<>(sessionDTOList, HttpStatus.OK);
    }

    @GetMapping("/findAllSessionsByLoggedInUser")
    @ApiOperation(value = "${SessionController.findAllByUser}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A list of sessions from user has been loaded")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<SessionDTO>> loadSessionListByUser(HttpServletRequest request) throws CustomException {
        User user = userService.whoami(request);
        List<SessionDTO> sessionDTOList = new ArrayList<>();
        List<Session> sessionList = sessionService.getAllSessionsByUser(user.getId());
        for(Session session : sessionList){
            SessionDTO sessionDTO = modelMapper.map(session, SessionDTO.class);
            sessionDTO.setState(sessionService.checkState(session.getId()));
            sessionDTOList.add(sessionDTO);
        }
        return new ResponseEntity<>(sessionDTOList, HttpStatus.OK);
    }

    @GetMapping("/findAllActiveSessionsOfUser/{id}")
    @ApiOperation(value = "${SessionController.findAllActiveOfUser}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A list of active sessions from user has been loaded")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<SessionDTO>> loadActiveSessionListByUser(HttpServletRequest request) throws CustomException {
        User user = userService.whoami(request);
        List<SessionDTO> sessionDTOList = new ArrayList<>();
        List<Session> sessionList = sessionService.getAllActiveSessionsOfUser(user.getId());
        for(Session session : sessionList){
            SessionDTO sessionDTO = modelMapper.map(session, SessionDTO.class);
            sessionDTOList.add(sessionDTO);
        }
        return new ResponseEntity<>(sessionDTOList, HttpStatus.OK);
    }

    @GetMapping("/findAllPlannedSessionsOfUser/{id}")
    @ApiOperation(value = "${SessionController.findAllPlannedOfUser}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A list of planned sessions from user has been loaded")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<SessionDTO>> loadPlannedSessionListByUser(HttpServletRequest request) throws CustomException {
        User user = userService.whoami(request);
        List<SessionDTO> sessionDTOList = new ArrayList<>();
        List<Session> sessionList = sessionService.getAllPlannedSessionsOfUser(user.getId());
        for(Session session : sessionList){
            SessionDTO sessionDTO = modelMapper.map(session, SessionDTO.class);
            sessionDTOList.add(sessionDTO);
        }
        return new ResponseEntity<>(sessionDTOList, HttpStatus.OK);
    }

    @GetMapping("/findAllPassedSessionsOfUser/{id}")
    @ApiOperation(value = "${SessionController.findAllPassedOfUser}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A list of passed sessions from user has been loaded")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<SessionDTO>> loadPassedSessionListByUser(HttpServletRequest request) throws CustomException {
        User user = userService.whoami(request);
        List<SessionDTO> sessionDTOList = new ArrayList<>();
        List<Session> sessionList = sessionService.getAllPassedSessionsOfUser(user.getId());
        for(Session session : sessionList){
            SessionDTO sessionDTO = modelMapper.map(session, SessionDTO.class);
            sessionDTOList.add(sessionDTO);
        }
        return new ResponseEntity<>(sessionDTOList, HttpStatus.OK);
    }

    @GetMapping("/end/{id}")
    @ApiOperation(value = "${SessionController.end}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A session has been ended")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<SessionDTO> endSession(@PathVariable(value = "id") Long id) throws CustomException {
        Session session = sessionService.load(id);
        session.setNotificationMessage("Session ended by organisor");
        Session savedSession = sessionService.endSession(id);
        return new ResponseEntity<>(modelMapper.map(savedSession, SessionDTO.class), HttpStatus.OK);
    }

    @GetMapping("/start/{id}")
    @ApiOperation(value = "${SessionController.start}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A new session has been started")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<GameSessionDTO> startSession(@PathVariable(value = "id") Long id) throws CustomException {
        Session session = sessionService.startSession(id);
        return new ResponseEntity<>(modelMapper.map(session, GameSessionDTO.class), HttpStatus.OK);
    }


    @CrossOrigin(origins = {"http://localhost:4200"})
    @PostMapping("/selectCard/{id}")
    @ApiOperation(value = "${SessionController.turn}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A card has been selected")
    })
    public ResponseEntity<GameSessionDTO> SelectCard(@PathVariable(value = "id") Long sessionId, @Valid @RequestBody CardDTO cardDTO, HttpServletRequest req) throws CustomException {
        User user = userService.whoami(req);
        Session session = sessionService.cardSelectionChange( modelMapper.map(cardDTO, Card.class), sessionId, user);
        return new ResponseEntity<>(modelMapper.map(session, GameSessionDTO.class), HttpStatus.OK);


    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/skipTurnCardSelection")
    @ApiOperation(value = "${SessionController.turn}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "Skipped select turn")
    })
    public ResponseEntity<GameSessionDTO> SkipSelectCardTurn(@RequestBody Long sessionId, HttpServletRequest req) throws CustomException {
        Session session = this.sessionService.load(sessionId);
        User user = userService.whoami(req);
        Session sesionOut = this.sessionService.cardSelectedChangeTurn(session, user);
        return new ResponseEntity<>(modelMapper.map(sesionOut, GameSessionDTO.class), HttpStatus.OK);
    }


 /*   @PutMapping("/turn/{id}")
    @ApiOperation(value = "${SessionController.turn}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong")
            //@ApiResponse(code = 200, message = "A new card has been created")
    })
    @CrossOrigin(origins = "http://localhost:4200", maxAge = 3000)
    public ResponseEntity<GameSessionDTO> takeTurn(@PathVariable(value = "id") Long id, HttpServletRequest request) throws CustomException {
        Session session = cardSessionService.load(id).getSession();
        User user = userService.whoami(request);
        session = sessionService.takeTurnWithChecks(id, session, user);
        return new ResponseEntity<>(modelMapper.map(session, GameSessionDTO.class), HttpStatus.OK);
    }*/

    @CrossOrigin(origins = {"http://localhost:4200"}, maxAge = 3000)
    @PutMapping("/playCard")
    @ApiOperation(value = "${SessionController.turn}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "Card Played")
    })
    public ResponseEntity<GameSessionDTO> playCard(@RequestBody Long cardSessionId, HttpServletRequest req) throws CustomException {
        Session session = cardSessionService.load(cardSessionId).getSession();
        User user = userService.whoami(req);
        session = sessionService.takeTurnWithChecks(cardSessionId, session, user);
        return new ResponseEntity<>(modelMapper.map(session, GameSessionDTO.class), HttpStatus.OK);
    }

    @GetMapping("/changeTurn/{id}")
    @ApiOperation(value = "${SessionController.changeTurn}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "Turn changed")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<GameSessionDTO> changeUserTurn(@PathVariable(value = "id") Long id) throws CustomException {
        Session session = sessionService.changeUserTurn(id);
        return new ResponseEntity<>(modelMapper.map(session, GameSessionDTO.class), HttpStatus.OK);
    }

    @GetMapping("/findGameSession/{id}")
    @ApiOperation(value = "${SessionController.findGameSession}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "GameSession returned")
    })
    public ResponseEntity<GameSessionDTO> findGameSessionById(@PathVariable(value = "id") Long sessionId) throws CustomException {
        Session session = sessionService.load(sessionId);
        return new ResponseEntity<>(modelMapper.map(session, GameSessionDTO.class), HttpStatus.OK);
    }

    @PostMapping("/endSelectionPhaseSession")
    @ApiOperation(value = "${SessionController.findGameSession}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "Selection phase ended")
    })
    public ResponseEntity<GameSessionDTO> endSelectionPhaseSession(@RequestBody long sessionId, HttpServletRequest req) throws CustomException {
        Session session = sessionService.load(sessionId);
        User user = userService.whoami(req);
        Session sessionOut = sessionService.endSelectionPhase(session, user);
        return new ResponseEntity<>(modelMapper.map(sessionOut, GameSessionDTO.class), HttpStatus.OK);
    }

    @GetMapping("/cardSelection/{id}")
    @ApiOperation(value = "${SessionController.select}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "All cards converted to cardsessions")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<SessionDTO> selectCardsForSession(@PathVariable(value = "id") Long id, @RequestParam List<Long> cardList) throws CustomException {
        List<CardSession> cardSessionList = cardSessionService.getUserCardSessionForSession(cardList, sessionService.load(id));
        Session savedSession = sessionService.save(sessionService.load(id));
        return new ResponseEntity<>(modelMapper.map(savedSession, SessionDTO.class), HttpStatus.OK);
    }

    @PutMapping("/cardCreation/{id}")
    @ApiOperation(value = "${SessionController.userCreate}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "Created a new cardsession for a session")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<SessionDTO> createCardsForSession(@PathVariable(value = "id") Long id, @Valid @RequestBody List<CardSessionDTO> cardList) throws CustomException {
        List<CardSession> cardSessionList = new ArrayList<>();
        for (CardSessionDTO cardSessionDTO : cardList){
            CardSession cardSession = modelMapper.map(cardSessionDTO, CardSession.class);
            cardSessionList.add(cardSession);
        }
        List<CardSession> returnedCardSessionList = cardSessionService.addUserCardSessionForSession(cardSessionList, sessionService.load(id));
        Session savedSession = sessionService.save(sessionService.load(id));
        return new ResponseEntity<>(modelMapper.map(savedSession, SessionDTO.class), HttpStatus.OK);
    }

    @GetMapping("/findAllActiveSessionsWithCardsSelectedOfUser")
    @ApiOperation(value = "${SessionController.findAllActiveOfUser}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "Loaded session with selected cards")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<SessionDTO>> loadActiveSessionListWithCardsSelectedByUser(HttpServletRequest request) throws CustomException {
        User user = userService.whoami(request);
        List<SessionDTO> sessionDTOList = new ArrayList<>();
        List<Session> sessionList = sessionService.getAllActiveSessionsWithSelectedCardsOfUser(user.getId());
        for(Session session : sessionList){
            SessionDTO sessionDTO = modelMapper.map(session, SessionDTO.class);
            sessionDTOList.add(sessionDTO);
        }
        return new ResponseEntity<>(sessionDTOList, HttpStatus.OK);
    }
}