package be.kdg.kandoebackend.controller.rest;

import be.kdg.kandoebackend.dto.SnapshotDTO;
import be.kdg.kandoebackend.dto.SnapshotTurnDTO;
import be.kdg.kandoebackend.error.CustomException;
import be.kdg.kandoebackend.model.Snapshot;
import be.kdg.kandoebackend.model.SnapshotTurn;
import be.kdg.kandoebackend.service.SnapshotService;
import io.swagger.annotations.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/snapshots")
@Api(tags = "snapshots")
public class SnapshotRestController {

    private final SnapshotService snapshotService;
    private final ModelMapper modelMapper;

    @Autowired
    public SnapshotRestController(SnapshotService snapshotService, ModelMapper modelMapper) {
        this.snapshotService = snapshotService;
        this.modelMapper = modelMapper;
    }

    @PostMapping("/save")
    @ApiOperation(value = "${SnapshotController.save}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A new snapshot has been created")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<SnapshotDTO> createSnapshot(@RequestBody SnapshotDTO snapshotDTO){
        Snapshot snapshotIn = modelMapper.map(snapshotDTO, Snapshot.class);
        Snapshot snapshotOut = snapshotService.save(snapshotIn);
        return new ResponseEntity<>(modelMapper.map(snapshotOut, SnapshotDTO.class), HttpStatus.CREATED);
    }

    @GetMapping("/findSnapshot/{id}")
    @ApiOperation(value = "${SnapshotController.find}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A snapshot has been loaded")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<SnapshotDTO> loadSnapshot(@PathVariable(value = "id") Long id) throws CustomException {
        Snapshot snapshot = snapshotService.loadSnapshot(id);
        return new ResponseEntity<>(modelMapper.map(snapshot, SnapshotDTO.class), HttpStatus.OK);
    }

    @GetMapping("/findSnapshotTurn/{id}")
    @ApiOperation(value = "${SnapshotController.findSnapshotTurn}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A snapshotTurn has been loaded")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<SnapshotTurnDTO> loadSnapshotTurn(@PathVariable(value = "id") Long id) throws CustomException {
        SnapshotTurn snapshotTurn = snapshotService.loadSnapshotTurn(id);
        return new ResponseEntity<>(modelMapper.map(snapshotTurn, SnapshotTurnDTO.class), HttpStatus.OK);
    }

    @GetMapping("/findAllSnapshotsBySession/{id}")
    @ApiOperation(value = "${SnapshotController.findAllBySession}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "All snapshots have been loaded for session")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<SnapshotDTO>> loadAllSnapshotsBySession(@PathVariable(value = "id") Long id) throws CustomException {
        List<SnapshotDTO> snapshotDTOList = new ArrayList<>();
        List<Snapshot> snapshotList = snapshotService.getSnapshotBySession(id);
        for(Snapshot snapshot : snapshotList){
            SnapshotDTO snapshotDTO = modelMapper.map(snapshot, SnapshotDTO.class);
            snapshotDTOList.add(snapshotDTO);
        }
        return new ResponseEntity<>(snapshotDTOList, HttpStatus.OK);
    }

    @GetMapping("/findAllSnapshotTurnsBySnapshot/{id}")
    @ApiOperation(value = "${SnapshotController.findAllBySnapshot}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "All snapshotTurns for snapshot have been loaded")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<SnapshotTurnDTO>> loadAllSnapshotTurnsBySnapshot(@PathVariable(value = "id") Long id) throws CustomException {
        List<SnapshotTurnDTO> snapshotTurnDTOList = new ArrayList<>();
        List<SnapshotTurn> snapshotTurnList = snapshotService.getAllSnapshotTurnsBySnapshot(id);
        for(SnapshotTurn snapshotTurn : snapshotTurnList){
            SnapshotTurnDTO snapshotTurnDTO = modelMapper.map(snapshotTurn, SnapshotTurnDTO.class);
            snapshotTurnDTOList.add(snapshotTurnDTO);
        }
        return new ResponseEntity<>(snapshotTurnDTOList, HttpStatus.OK);
    }
}
