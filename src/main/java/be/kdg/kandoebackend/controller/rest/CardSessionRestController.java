package be.kdg.kandoebackend.controller.rest;

import be.kdg.kandoebackend.dto.CardSessionDTO;
import be.kdg.kandoebackend.error.CustomException;
import be.kdg.kandoebackend.model.Card;
import be.kdg.kandoebackend.model.CardSession;
import be.kdg.kandoebackend.model.Session;
import be.kdg.kandoebackend.service.CardSessionService;
import be.kdg.kandoebackend.service.SessionService;
import io.swagger.annotations.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/cardSessions")
@Api(tags = "cardSessions")
@CrossOrigin(origins = "http://localhost:4200")
public class CardSessionRestController {
    private final CardSessionService cardSessionService;
    private final SessionService sessionService;
    private final ModelMapper modelMapper;

    @Autowired
    public CardSessionRestController(CardSessionService cardSessionService, SessionService sessionService, ModelMapper modelMapper) {
        this.cardSessionService = cardSessionService;
        this.sessionService = sessionService;
        this.modelMapper = modelMapper;
    }

    @PostMapping("/save")
    @ApiOperation(value = "${CardSessionController.save}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "The name is either to short or too long! ( minsize : 2, maxsize: 20 )"),
            @ApiResponse(code = 422, message = "Card already exists"),
            @ApiResponse(code = 200, message = "A new cardSession has been created")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<CardSessionDTO> createCardSession(@Valid @RequestBody CardSessionDTO cardSessionDTO){
        CardSession cardSessionIn = modelMapper.map(cardSessionDTO, CardSession.class);
        CardSession cardSessionOut = cardSessionService.save(cardSessionIn);
        return new ResponseEntity<>(modelMapper.map(cardSessionOut, CardSessionDTO.class), HttpStatus.CREATED);
    }

    @GetMapping("/find/{id}")
    @ApiOperation(value = "${CardSessionController.find}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A new cardSession has been returned")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<CardSessionDTO> loadCardSession(@PathVariable(value = "id") Long id)  throws CustomException {
        CardSession cardSession = cardSessionService.load(id);
        return new ResponseEntity<>(modelMapper.map(cardSession, CardSessionDTO.class), HttpStatus.OK);
    }

    @GetMapping("/createCardSessionsForCardsInTheme/{id}")
    @ApiOperation(value = "${CardSessionController.cardsession}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A new cardSession has been created from card")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<CardSessionDTO>> createCardSessionFromCard (@PathVariable(value = "id") Long id) throws CustomException {
        List<CardSessionDTO> cardSessionDTOList = new ArrayList<>();
        Session session = sessionService.load(id);
        List<Card> cardList = session.getTheme().getCardList();
        for(Card card : cardList){
            CardSession cardSession = cardSessionService.addCardToSession(card, session);
            CardSessionDTO cardSessionDTO = modelMapper.map(cardSession, CardSessionDTO.class);
            cardSessionDTOList.add(cardSessionDTO);
        }
        return new ResponseEntity<>(cardSessionDTOList, HttpStatus.OK);
    }

    @PutMapping("/incrementPriority/{id}")
    @ApiOperation(value = "${CardSessionController.increment}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "Priority incremented")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<CardSessionDTO> incrementPriority(@PathVariable(value = "id") Long id) throws CustomException {
        CardSession cardSession = cardSessionService.incrementPriority(id);
        return new ResponseEntity<>(modelMapper.map(cardSession, CardSessionDTO.class), HttpStatus.OK);
    }

    @GetMapping("/findAllCardSessionsBySession/{id}")
    @ApiOperation(value = "${SessionController.findAllCardSessionsBySession}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "All cardsessions for a session have been returned")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<CardSessionDTO>> findAllCardSessionsBySession(@PathVariable(value = "id") Long id) throws CustomException {
        List<CardSession> cardSessionList = cardSessionService.getCardSessionsForSession(id);
        List<CardSessionDTO> cardSessionDTOList = new ArrayList<CardSessionDTO>();
        for (CardSession cardSession : cardSessionList) {
            CardSessionDTO cardSessionDTO = modelMapper.map(cardSession, CardSessionDTO.class);
            cardSessionDTO.setSessionId(cardSession.getSession().getId());
            cardSessionDTOList.add(cardSessionDTO);
        }
        return new ResponseEntity<>(cardSessionDTOList, HttpStatus.OK);
    }

    @DeleteMapping("/removeCardSession/{id}")
    @ApiOperation(value = "${CardSessionController.removeCardSession}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A new cardsession has been deleted")
    })
    public void deleteCardSession(@PathVariable(value = "id") Long id) throws CustomException {
        cardSessionService.removeCardSession(id);
    }
}