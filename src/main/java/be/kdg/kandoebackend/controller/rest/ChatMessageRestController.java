package be.kdg.kandoebackend.controller.rest;

import be.kdg.kandoebackend.dto.ChatMessageDTO;
import be.kdg.kandoebackend.error.CustomException;
import be.kdg.kandoebackend.model.ChatMessage;
import be.kdg.kandoebackend.service.ChatMessageService;
import io.swagger.annotations.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/chatMessages")
@Api(tags = "chatMessages")
@CrossOrigin(origins = "http://localhost:4200")
public class ChatMessageRestController {
    private final ChatMessageService chatMessageService;
    private final ModelMapper modelMapper;

    @Autowired
    public ChatMessageRestController(ChatMessageService chatMessageService, ModelMapper modelMapper) {
        this.chatMessageService = chatMessageService;
        this.modelMapper = modelMapper;
    }

    @PostMapping("/save")
    @ApiOperation(value = "${ChatMessageController.createSession}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A chatmessage has been created")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<ChatMessageDTO> createChatMessage(@ApiParam("Signup ApplicationUser") @Valid @RequestBody ChatMessageDTO chatMessageDTO){
        ChatMessage chatMessageIn = modelMapper.map(chatMessageDTO, ChatMessage.class);
        ChatMessage chatMessageOut = chatMessageService.save(chatMessageIn);
        return new ResponseEntity<>(modelMapper.map(chatMessageOut, ChatMessageDTO.class), HttpStatus.CREATED);
    }

    @GetMapping("/findChatBySession/{id}")
    @ApiOperation(value = "${ChatMessageController.find}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "All chatmessages have been loaded")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<ChatMessageDTO>> loadAllChatMessages(@PathVariable(value = "id") Long id)  throws CustomException {
        List<ChatMessage> chatMessageList = chatMessageService.loadChatForSession(id);
        List<ChatMessageDTO> chatMessageDTOList = new ArrayList<>();
        for(ChatMessage chatMessage : chatMessageList){
            ChatMessageDTO chatMessageDTO = modelMapper.map(chatMessage, ChatMessageDTO.class);
            chatMessageDTOList.add(chatMessageDTO);
        }
        return new ResponseEntity<>(chatMessageDTOList, HttpStatus.OK);
    }
}