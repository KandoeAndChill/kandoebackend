package be.kdg.kandoebackend.controller.rest;

import be.kdg.kandoebackend.dto.ThemeDTO;
import be.kdg.kandoebackend.error.CustomException;
import be.kdg.kandoebackend.model.Theme;
import be.kdg.kandoebackend.model.User;
import be.kdg.kandoebackend.service.ThemeService;
import be.kdg.kandoebackend.service.UserService;
import io.swagger.annotations.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/themes")
@Api(tags = "themes")
@CrossOrigin(origins = "http://localhost:4200")
public class ThemeRestController {
    private final ThemeService themeService;
    private ModelMapper modelMapper = new ModelMapper();
    private final UserService userService;

    @Autowired
    public ThemeRestController(ThemeService themeService, UserService userService){
        this.themeService = themeService;
        this.userService = userService;
    }

    @PostMapping("/save")
    @ApiOperation(value = "${ThemeController.create}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A new theme has been created")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<ThemeDTO> createTheme(@ApiParam("Adding Theme") @Valid @RequestBody ThemeDTO themeDTO, HttpServletRequest req) {
        User user = userService.whoami(req);
        themeDTO.setUserId(user.getId());
        Theme themeIn = modelMapper.map(themeDTO, Theme.class);
        Theme themeOut = themeService.save(themeIn);
        return new ResponseEntity<>(modelMapper.map(themeOut, ThemeDTO.class), HttpStatus.CREATED);
    }

    @PutMapping("/edit/{id}")
    @ApiOperation(value = "${ThemeController.edit}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A theme has been edited")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<ThemeDTO> editTheme(@ApiParam("Editing Theme") @PathVariable(value = "id") Long id, @RequestBody ThemeDTO themeDTO) {
        Theme themeUpdate = themeService.load(id);
        themeService.updateThemeName(themeUpdate, themeDTO.getName());
        themeService.updateThemeDescription(themeUpdate, themeDTO.getDescription());
        return new ResponseEntity<>(modelMapper.map(themeUpdate, ThemeDTO.class), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "${ThemeController.delete}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A theme has been deleted")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteTheme(@ApiParam("Id") @PathVariable(value = "id") Long id) throws CustomException {
        Theme toGo = themeService.load(id);
        themeService.delete(toGo.getId());
    }

    @GetMapping("/find/{id}")
    @ApiOperation(value = "${ThemeController.find}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "A theme has been loaded")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<ThemeDTO> loadTheme(@PathVariable(value = "id") Long id) throws CustomException {
        Theme theme = themeService.load(id);
        return new ResponseEntity<>(modelMapper.map(theme, ThemeDTO.class), HttpStatus.OK);
    }

    @GetMapping("/findAllByLoggedInUser")
    @ApiOperation(value = "${ThemeController.findByUser}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 200, message = "All themes loaded for user")
    })
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<ThemeDTO>> loadAllThemesByUser(HttpServletRequest request) throws CustomException {
        User user = userService.whoami(request);
        List<Theme> themeList = themeService.getAllThemesByUserId(user.getId());
        List<ThemeDTO> themeDTOList = new ArrayList<>();
        for(Theme theme : themeList){
            ThemeDTO themeDTO = modelMapper.map(theme, ThemeDTO.class);
            themeDTOList.add(themeDTO);
        }
        return new ResponseEntity<>(themeDTOList, HttpStatus.OK);
    }
}