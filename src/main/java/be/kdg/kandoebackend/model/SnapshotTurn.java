package be.kdg.kandoebackend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class SnapshotTurn {
    @Id
    @GeneratedValue
    @Column(name = "snapshotTurnId")
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "snapshotId")
    private Snapshot snapshot;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cardSessionId")
    private CardSession selectedCard;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private User activeUser;
    private int currentRound;
    private boolean automaticShot;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Snapshot getSnapshot() {
        return snapshot;
    }

    public void setSnapshot(Snapshot snapshot) {
        this.snapshot = snapshot;
    }

    public CardSession getSelectedCard() {
        return selectedCard;
    }

    public void setSelectedCard(CardSession selectedCard) {
        this.selectedCard = selectedCard;
    }

    public User getActiveUser() {
        return activeUser;
    }

    public void setActiveUser(User activeUser) {
        this.activeUser = activeUser;
    }

    public int getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(int currentRound) {
        this.currentRound = currentRound;
    }

    public boolean isAutomaticShot() {
        return automaticShot;
    }

    public void setAutomaticShot(boolean automaticShot) {
        this.automaticShot = automaticShot;
    }
}
