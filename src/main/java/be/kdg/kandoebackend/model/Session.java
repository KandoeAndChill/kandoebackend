package be.kdg.kandoebackend.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.beans.factory.annotation.Value;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@AllArgsConstructor
public class Session {
    @Id
    @GeneratedValue
    @Column(name = "sessionId", nullable = false)
    private Long id;
    @Column
    private String name;
    @Column
    private boolean chanceCircle;
    @Column
    @Value("false")
    private boolean gameOver;
    @Value("false")
    private boolean cardsSelected;
    @Column
    private int numberOfCircles;
    @Column
    private int minCards;
    @Column
    private int maxCards;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organiserId")
    private User organiser;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "activeUserId")
    private User activeUser;
    @Column
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss.SSS")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime startDate;
    private Long snapshotId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "themeId")
    private Theme theme;
    @OneToMany(
            mappedBy = "session",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<CardSession> cardList;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "userSession",
            joinColumns = @JoinColumn(name = "sessionId", referencedColumnName = "sessionId"),
            inverseJoinColumns = @JoinColumn(name = "userId", referencedColumnName = "userId"))
    @Fetch(FetchMode.SELECT)
    private List<User> userList;
    @Column
    @Value("false")
    private boolean userCardsEnabled;
    @Column
    private int numberOfRounds;
    @Column
    private int currentRound;
    @OneToMany(
            mappedBy = "session",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<ChatMessage> chatMessageList;
    private String notificationMessage;
    private int roundTimeInMinutes;

    public Session() {
        userList = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChanceCircle() {
        return chanceCircle;
    }

    public void setChanceCircle(boolean chanceCircle) {
        this.chanceCircle = chanceCircle;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public int getNumberOfCircles() {
        return numberOfCircles;
    }

    public void setNumberOfCircles(int numberOfCircles) {
        this.numberOfCircles = numberOfCircles;
    }

    public int getMinCards() {
        return minCards;
    }

    public void setMinCards(int minCards) {
        this.minCards = minCards;
    }

    public int getMaxCards() {
        return maxCards;
    }

    public void setMaxCards(int maxCards) {
        this.maxCards = maxCards;
    }

    public User getOrganiser() {
        return organiser;
    }

    public void setOrganiser(User organiser) {
        this.organiser = organiser;
    }

    public User getActiveUser() {
        return activeUser;
    }

    public void setActiveUser(User activeUser) {
        this.activeUser = activeUser;
    }

    public Long getSnapshotId() {
        return snapshotId;
    }

    public void setSnapshotId(Long snapshotId) {
        this.snapshotId = snapshotId;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public List<CardSession> getCardList() {
        return cardList;
    }

    public void setCardList(List<CardSession> cardList) {
        this.cardList = cardList;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public void addToUserList(User user){
        this.userList.add(user);
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        if(startDate != null){
            this.startDate = startDate;
        } else {
            this.startDate = LocalDateTime.now();
        }
    }

    public boolean isUserCardsEnabled() {
        return userCardsEnabled;
    }

    public void setUserCardsEnabled(boolean userCardsEnabled) {
        this.userCardsEnabled = userCardsEnabled;
    }

    public int getNumberOfRounds() {
        return numberOfRounds;
    }

    public void setNumberOfRounds(int numberOfRounds) {
        this.numberOfRounds = numberOfRounds;
    }

    public int getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(int currentRound) {
        this.currentRound = currentRound;
    }

    public List<ChatMessage> getChatMessageList() {
        return chatMessageList;
    }

    public void setChatMessageList(List<ChatMessage> chatMessageList) {
        this.chatMessageList = chatMessageList;
    }

    public String getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }

    public int getRoundTimeInMinutes() {
        return roundTimeInMinutes;
    }

    public void setRoundTimeInMinutes(int roundTimeInMinutes) {
        this.roundTimeInMinutes = roundTimeInMinutes;
    }

    public boolean isCardsSelected() {
        return cardsSelected;
    }

    public void setCardsSelected(boolean cardsSelected) {
        this.cardsSelected = cardsSelected;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Session session = (Session) o;
        return chanceCircle == session.chanceCircle &&
                numberOfCircles == session.numberOfCircles &&
                minCards == session.minCards &&
                maxCards == session.maxCards &&
                Objects.equals(id, session.id) &&
                Objects.equals(name, session.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, chanceCircle, numberOfCircles, minCards, maxCards);
    }

    @Override
    public String toString() {
        return "Session{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", isChanceCircle=" + chanceCircle +
                ", numberOfCircles=" + numberOfCircles +
                ", minCards=" + minCards +
                ", maxCards=" + maxCards +
                ", theme=" + theme.getId() +
                /*", startTime=" + theme.getStartTime().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss.SSS") +*/
                '}';
    }
}
