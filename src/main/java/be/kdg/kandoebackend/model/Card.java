package be.kdg.kandoebackend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Card {
    @Id
    @GeneratedValue
    @Column(name = "cardId")
    private Long id;
    @Column
    private String name;
    @Column
    @Lob
    private String imageBase64Url;
    @OneToMany(
            mappedBy = "card",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Rating> ratings = new ArrayList<>();
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "themeId")
    private Theme theme;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageBase64Url() {
        return imageBase64Url;
    }

    public void setImageBase64Url(String imageBase64Url) {
        this.imageBase64Url = imageBase64Url;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public void addRating(Rating rating){
        ratings.add(rating);
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return Objects.equals(id, card.id) &&
                Objects.equals(name, card.name) &&
                Objects.equals(imageBase64Url, card.imageBase64Url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, imageBase64Url);
    }

    @Override
    public String toString() {
        return "Card{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", imageBase64Url='" + imageBase64Url + '\'' +
                ", themeId='" + theme.getId() + '\'' +
                '}';
    }
}