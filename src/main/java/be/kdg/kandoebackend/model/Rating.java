package be.kdg.kandoebackend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Rating {
    @Id
    @GeneratedValue
    @Column(name = "ratingId")
    private long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cardId")
    private Card card;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private User user;
    private String comment;
}
