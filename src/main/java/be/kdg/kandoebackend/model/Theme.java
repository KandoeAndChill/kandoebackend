package be.kdg.kandoebackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Theme {

    @Id
    @GeneratedValue
    @Column(name = "themeId")
    private Long id;
    @Column
    @NotEmpty
    private String name;
    @Column
    private String description;
    @Column
    private boolean commentaryAllowed;
    @OneToMany(
            mappedBy = "theme",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Card> cardList;
    @JsonIgnore
    @OneToMany(
            mappedBy = "theme",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Session> sessionList;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name="theme_tag", joinColumns=@JoinColumn(name="themeId"), inverseJoinColumns=@JoinColumn(name="tagId"))
    private List<Tag> tagList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private User user;

    public Theme(String name, String description, List<Tag> tagList)  {
        this.name = name;
        this.description = description;
        this.commentaryAllowed = false;
        this.cardList = new ArrayList<>();
        this.sessionList = new ArrayList<>();
        this.tagList = tagList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isCommentaryAllowed() {
        return commentaryAllowed;
    }

    public void setCommentaryAllowed(boolean commentaryAllowed) {
        this.commentaryAllowed = commentaryAllowed;
    }

    public List<Session> getSessionList() {
        return sessionList;
    }

    public void setSessionList(List<Session> sessionList) {
        this.sessionList = sessionList;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public List<Card> getCardList() {
        return cardList;
    }

    public void setCardList(List<Card> cardList) {
        this.cardList = cardList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Theme theme = (Theme) o;
        return Objects.equals(id, theme.id) &&
                Objects.equals(name, theme.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}