package be.kdg.kandoebackend.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.Objects;

@Entity
@NoArgsConstructor
@AllArgsConstructor
public class CardSession {
    @Id
    @GeneratedValue
    @Column(name = "cardSessionId")
    private Long id;
    @Column
    private String cardText;
    @Column
    @Lob
    private String imageUrl;
    @Column
    private int priority;
    @ManyToOne
    @JoinColumn(name = "sessionId")
    private Session session;
    private int snapshotPriority;

    public CardSession(String cardText, String imageUrl, Session session){
        this.cardText = cardText;
        this.imageUrl = imageUrl;
        this.session = session;
        this.priority = 0;
        this.snapshotPriority = 0;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCardText() {
        return cardText;
    }

    public void setCardText(String cardText) {
        this.cardText = cardText;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getPriority() {
        return priority;
    }

    public void increasePriority() {
        this.priority += 1;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public int getSnapshotPriority() {
        return snapshotPriority;
    }

    public void increaseSnapshotPriority(int snapshotPriority) {
        this.snapshotPriority += 1;
    }

    public void setSnapshotPriority(int snapshotPriority) {
        this.snapshotPriority = snapshotPriority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CardSession that = (CardSession) o;
        return  Objects.equals(getSession().getId(), that.getSession().getId()) &&
                Objects.equals(cardText, that.cardText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cardText, imageUrl, priority);
    }

    @Override
    public String toString() {
        return "CardSession{" +
                "id=" + id +
                ", cardText='" + cardText + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", priority=" + priority +
                ", session=" + session.getId() +
                '}';
    }
}