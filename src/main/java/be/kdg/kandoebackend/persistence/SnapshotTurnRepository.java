package be.kdg.kandoebackend.persistence;

import be.kdg.kandoebackend.model.Session;
import be.kdg.kandoebackend.model.Snapshot;
import be.kdg.kandoebackend.model.SnapshotTurn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SnapshotTurnRepository extends JpaRepository<SnapshotTurn, Long> {
    List<SnapshotTurn> findAllBySnapshot(Snapshot snapshot);
}
