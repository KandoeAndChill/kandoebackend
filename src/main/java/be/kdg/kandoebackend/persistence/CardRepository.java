package be.kdg.kandoebackend.persistence;

import be.kdg.kandoebackend.model.Theme;
import org.springframework.data.jpa.repository.JpaRepository;
import be.kdg.kandoebackend.model.Card;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface CardRepository extends JpaRepository<Card, Long> {
    List<Card> findAllByTheme(Theme theme);
}
