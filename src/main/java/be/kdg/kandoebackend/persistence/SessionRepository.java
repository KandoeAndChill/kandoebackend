package be.kdg.kandoebackend.persistence;

import be.kdg.kandoebackend.model.Session;
import be.kdg.kandoebackend.model.Theme;
import be.kdg.kandoebackend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import java.time.LocalDateTime;
import java.util.List;

public interface SessionRepository extends JpaRepository<Session, Long> {
    List<Session> findAllByTheme(Theme theme);
    List<Session> findAllByThemeAndGameOver(Theme theme, boolean isOver);
    List<Session> findAllByThemeAndGameOverAndStartDateBefore (Theme theme, boolean isOver, LocalDateTime localDateTime);
    List<Session> findAllByThemeAndGameOverAndStartDateAfter (Theme theme, boolean isOver, LocalDateTime localDateTime);
    List<Session> findAllByOrganiser(User organiser);
    List<Session> findAllByUserListContaining(User user);
    List<Session> findAllByUserListContainingAndGameOver(User user, boolean isOver);
    List<Session> findAllByUserListContainingAndGameOverAndStartDateBefore(User user, boolean isOver, LocalDateTime localDateTime);
    List<Session> findAllByUserListContainingAndGameOverAndStartDateAfter(User user, boolean isOver, LocalDateTime localDateTime);
    List<Session> findAllByUserListContainingAndGameOverAndStartDateBeforeAndCardsSelected (User user, boolean isOver, LocalDateTime localDateTime, boolean cardsSelected);
    List<Session> findAllByGameOverAndStartDateBetween(boolean gameOver, LocalDateTime localDateTime, LocalDateTime localDateTime2);
}
