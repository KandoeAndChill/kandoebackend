package be.kdg.kandoebackend.persistence;

import be.kdg.kandoebackend.model.Theme;
import be.kdg.kandoebackend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ThemeRepository extends JpaRepository<Theme, Long> {
    List<Theme> findAllByUser(User user);
}
