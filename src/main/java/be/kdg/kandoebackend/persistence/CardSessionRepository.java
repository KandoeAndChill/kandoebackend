package be.kdg.kandoebackend.persistence;

import be.kdg.kandoebackend.model.CardSession;
import be.kdg.kandoebackend.model.Session;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CardSessionRepository extends JpaRepository<CardSession, Long> {
    List<CardSession> findAllBySession(Session session);
}
