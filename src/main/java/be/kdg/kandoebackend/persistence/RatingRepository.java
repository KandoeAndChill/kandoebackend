package be.kdg.kandoebackend.persistence;

import be.kdg.kandoebackend.model.Card;
import be.kdg.kandoebackend.model.Rating;
import be.kdg.kandoebackend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RatingRepository extends JpaRepository<Rating, Long> {
    List<Rating> findAllByUserAndCard(User user, Card card);
    List<Rating> findAllByCard(Card card);
}
