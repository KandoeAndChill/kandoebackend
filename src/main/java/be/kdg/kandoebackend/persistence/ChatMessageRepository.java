package be.kdg.kandoebackend.persistence;

import be.kdg.kandoebackend.model.ChatMessage;
import be.kdg.kandoebackend.model.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatMessageRepository extends JpaRepository<ChatMessage, Long> {
    List<ChatMessage> findAllBySession(Session session);
}
