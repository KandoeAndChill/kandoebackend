package be.kdg.kandoebackend.persistence;

import be.kdg.kandoebackend.model.Session;
import be.kdg.kandoebackend.model.Snapshot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface SnapshotRepository extends JpaRepository<Snapshot, Long> {
    List<Snapshot> findAllBySession(Session session);
}
