package be.kdg.kandoebackend.service;

import be.kdg.kandoebackend.error.CustomException;
import be.kdg.kandoebackend.model.Card;
import be.kdg.kandoebackend.model.Theme;
import be.kdg.kandoebackend.model.User;
import be.kdg.kandoebackend.persistence.CardRepository;
import be.kdg.kandoebackend.persistence.ThemeRepository;
import be.kdg.kandoebackend.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ThemeService {

    private final ThemeRepository themeRepository;
    private final CardRepository cardRepository;
    private final UserRepository userRepository;

    @Autowired
    public ThemeService(ThemeRepository themeRepository, CardRepository cardRepository, UserRepository userRepository){
        this.themeRepository = themeRepository;
        this.cardRepository = cardRepository;
        this.userRepository = userRepository;
    }

    public Theme save(Theme theme){
        return themeRepository.save(theme);
    }

    public Theme load(Long id) throws CustomException {
        Optional<Theme> optionalTheme = themeRepository.findById(id);
        if(optionalTheme.isPresent()){
            return optionalTheme.get();
        }
        throw new CustomException("Theme not found!", HttpStatus.NOT_FOUND);
    }

    public void delete(Long id) throws CustomException{
        Optional<Theme> optionalTheme = themeRepository.findById(id);
        if(optionalTheme.isPresent()){
            themeRepository.deleteById(id);
        }
        else{
            throw new CustomException("Theme not found", HttpStatus.NOT_FOUND);
        }
    }

    public Theme updateThemeName(Theme theme, String newname) {
        theme.setName(newname);
        return themeRepository.save(theme);
    }

    public Theme updateThemeDescription(Theme theme, String newDescription) {
        theme.setDescription(newDescription);
        return themeRepository.save(theme);
    }

    public Theme updateThemeCommentaryAllowed(Theme theme, Boolean commentaryAllowed) {
        theme.setCommentaryAllowed(commentaryAllowed);
        return themeRepository.save(theme);
    }

    public Optional<Theme> getThemeById(Long id) {
        return themeRepository.findById(id);
    }

    public List<Theme> getAllThemes() {
        return themeRepository.findAll();
    }

    public List<Theme> getAllThemesByUserId(Long userId) {
        Optional<User> optionalUser = userRepository.findById(userId);
        if(optionalUser.isPresent()){
            User user = optionalUser.get();
            return themeRepository.findAllByUser(user);
        }
        throw new CustomException("User nog found", HttpStatus.NOT_FOUND);
    }
    public Card addCard(Card card) {
        return cardRepository.save(card);
    }

    public void deleteCard(Long id) throws CustomException {
        Optional<Card> optionalCard = cardRepository.findById(id);
        if(optionalCard.isPresent()){
            cardRepository.deleteById(id);
        }
        throw new CustomException("Card not found", HttpStatus.NOT_FOUND);
    }
}
