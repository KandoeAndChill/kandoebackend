package be.kdg.kandoebackend.service;

import be.kdg.kandoebackend.error.CustomException;
import be.kdg.kandoebackend.model.User;
import be.kdg.kandoebackend.persistence.UserRepository;
import be.kdg.kandoebackend.security.JwtTokenProvider;
import io.swagger.annotations.ResponseHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserService {

    private final UserRepository userRepository;
    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;
    private final EmailService emailService;
    private final ThemeService themeService;

    @Autowired
    public UserService(UserRepository userRepository, JwtTokenProvider jwtTokenProvider, AuthenticationManager authenticationManager, PasswordEncoder passwordEncoder, EmailService emailService, ThemeService themeService) {
        this.userRepository = userRepository;
        this.jwtTokenProvider = jwtTokenProvider;
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
        this.emailService = emailService;
        this.themeService = themeService;
    }

    public String signin(String username, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            return jwtTokenProvider.createToken(username, userRepository.findByUsername(username).getRoles());
        } catch (AuthenticationException e) {
            throw new CustomException("Invalid username/password", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ResponseHeader()
    public String save(User user) {
        if (!userRepository.existsByEmail(user.getEmail()) && !userRepository.existsByUsername(user.getUsername())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            userRepository.save(user);
            emailService.sendSimpleMessage(user.getEmail(), "Welcome to Kandoe&Chill", "Welcome " + user.getUsername()
                    + ",\n\nWe would like to welcome you to the platform Kandoe&Chill.\nWe hope we can satisfy all your Kandoe-needs." +
                    "\n\nTogether, we Kandoe it,\nKandoe&Chill-team");
            return jwtTokenProvider.createToken(user.getUsername(), user.getRoles());
        } else {
            throw new CustomException("A user has already been created with this email/username!", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public User updateUser(long id, User user) {
        User newUser = userRepository.getOne(id);
        newUser.setEmail(user.getEmail());
        newUser.setAvatarBase64(user.getAvatarBase64());
        newUser.setUsername(user.getUsername());
        newUser.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(newUser);
        return newUser;
    }

    // returns current user if authenticated.
    public User whoami(HttpServletRequest req) {
        return userRepository.findByUsername(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(req)));
    }

    public User findUserByEmail(String email) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new CustomException("The applicationUser doesn't exist", HttpStatus.NOT_FOUND);
        }
        return user;
    }

    public boolean isMailRegistered(String mail){
        User user = userRepository.findByEmail(mail);
        if(user == null){
            return false;
        } else {
            return true;
        }
    }

    public Optional<User> getUserById(Long id) {
        return userRepository.findById(id);
    }

    public boolean isThemeOrganiser(int themeId, HttpServletRequest req) {
        User user = whoami(req);
        return themeService.getAllThemesByUserId(user.getId()).stream().filter(theme -> theme.getId() == themeId).collect(Collectors.toList()).size() > 0;
    }
}
