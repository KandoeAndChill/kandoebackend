package be.kdg.kandoebackend.service;

import be.kdg.kandoebackend.error.CustomException;
import be.kdg.kandoebackend.model.Card;
import be.kdg.kandoebackend.model.CardSession;
import be.kdg.kandoebackend.model.Session;
import be.kdg.kandoebackend.persistence.CardRepository;
import be.kdg.kandoebackend.persistence.CardSessionRepository;
import be.kdg.kandoebackend.persistence.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CardSessionService {

    private final CardSessionRepository cardSessionRepository;
    private final CardService cardService;
    private final SessionRepository sessionRepository;

    @Autowired
    public CardSessionService(CardSessionRepository cardSessionRepository, CardService cardService, SessionRepository sessionRepository) {
        this.cardSessionRepository = cardSessionRepository;
        this.cardService = cardService;
        this.sessionRepository = sessionRepository;
    }

    public CardSession save(CardSession cardSession) {
         // CardSession's equals method compares respective sessionId and cardtext.
        if(cardSessionRepository.findAllBySession(cardSession.getSession()).contains(cardSession)){
            throw new CustomException("CardSession with this name already exists for given session!", HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return cardSessionRepository.save(cardSession);
    }

    public CardSession load(Long id) {
        Optional<CardSession> optionalCardSession = cardSessionRepository.findById(id);
        if (optionalCardSession.isPresent()) {
            return optionalCardSession.get();
        }
        throw new CustomException("CardSession not found", HttpStatus.NOT_FOUND);
    }

    public CardSession addCardToSession(Card card, Session session) {
        CardSession cardSession = new CardSession(card.getName(), card.getImageBase64Url(), session);
        return save(cardSession);
    }

    public List<CardSession> getCardSessionsForSession(Long id) {
        Optional<Session> optionalSession = sessionRepository.findById(id);
        if (optionalSession.isPresent()) {
            Session session = optionalSession.get();

            return cardSessionRepository.findAllBySession(session);
        }
        throw new CustomException("Session not found for these CardSessions!", HttpStatus.NOT_FOUND);
    }

    public List<CardSession> getUserCardSessionForSession(List<Long> cardIds, Session session) {
        List<Card> cardList = new ArrayList<>();
        for (Long cardId : cardIds) {
            Card card = cardService.load(cardId);
            cardList.add(card);
        }

        List<CardSession> cardSessionList = new ArrayList<>();
        for (Card card : cardList) {
            CardSession cardSession = new CardSession(card.getName(), card.getImageBase64Url(), session);
            if (session.getCardList().stream().noneMatch(o -> o.getCardText().equals(cardSession.getCardText()))) {
                CardSession savedCardSession = save(cardSession);
                cardSessionList.add(savedCardSession);
                session.getCardList().add(savedCardSession);
            } else {
                throw new CustomException("A cardsession with this name already exists!", HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }
        return cardSessionList;
    }

    public List<CardSession> addUserCardSessionForSession(List<CardSession> cardSessions, Session session) {
        List<CardSession> cardSessionList = new ArrayList<>();
        for (CardSession cardSession : cardSessions) {
            if (session.getCardList().stream().noneMatch(o -> o.getCardText().equals(cardSession.getCardText()))) {
                CardSession savedCardSession = save(cardSession);
                cardSessionList.add(savedCardSession);
                session.getCardList().add(savedCardSession);
            }
        }
        return cardSessionList;
    }

    public CardSession incrementPriority(Long id) {
        Optional<CardSession> optionalCardSession = cardSessionRepository.findById(id);
        if (optionalCardSession.isPresent()) {
            CardSession cardSession = optionalCardSession.get();
            cardSession.increasePriority();
            return cardSessionRepository.save(cardSession);
        }
        throw new CustomException("Card not found", HttpStatus.NOT_FOUND);
    }

    public void removeCardSession(Long id) {
        CardSession cardToDelete = load(id);
        this.cardSessionRepository.delete(cardToDelete);
    }
}
