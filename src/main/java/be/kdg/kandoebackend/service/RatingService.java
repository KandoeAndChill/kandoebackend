package be.kdg.kandoebackend.service;

import be.kdg.kandoebackend.model.Card;
import be.kdg.kandoebackend.model.Rating;
import be.kdg.kandoebackend.model.User;
import be.kdg.kandoebackend.persistence.RatingRepository;
import org.checkerframework.checker.units.qual.C;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RatingService {

    private final RatingRepository ratingRepository;

    @Autowired
    public RatingService(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    public List<Rating> getCardRatingByUser(User user, Card card){
        return ratingRepository.findAllByUserAndCard(user, card);
    }

    public List<Rating> getRatingByCard(Card card) {
        return ratingRepository.findAllByCard(card);
    }
}
