package be.kdg.kandoebackend.service;

import be.kdg.kandoebackend.error.CustomException;
import be.kdg.kandoebackend.model.Rating;
import be.kdg.kandoebackend.model.Theme;
import be.kdg.kandoebackend.model.User;
import be.kdg.kandoebackend.persistence.CardRepository;
import be.kdg.kandoebackend.persistence.ThemeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import be.kdg.kandoebackend.model.Card;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CardService {

    private final CardRepository cardRepository;
    private final ThemeRepository themeRepository;
    private final RatingService ratingService;

    @Autowired
    public CardService(CardRepository cardRepository, ThemeRepository themeRepository, RatingService ratingService) {
        this.cardRepository = cardRepository;
        this.themeRepository = themeRepository;
        this.ratingService = ratingService;
    }

    public Card save(Card card) {
        return cardRepository.save(card);
    }

    public Card load(Long id) throws CustomException {
        Optional<Card> optionalCard = cardRepository.findById(id);
        if (optionalCard.isPresent()) {
            return optionalCard.get();
        }
        throw new CustomException("Card not found!", HttpStatus.NOT_FOUND);
    }

    public List<Card> getCardsForTheme(Long id) throws CustomException {
        Optional<Theme> optionalTheme = themeRepository.findById(id);
        if (optionalTheme.isPresent()) {
            Theme theme = optionalTheme.get();
            return cardRepository.findAllByTheme(theme);
        }
        throw new CustomException("No theme found", HttpStatus.NOT_FOUND);
    }

    public List<Rating> addRating(long cardId, Rating rating, User user) {
       if(ratingService.getCardRatingByUser(user, load(cardId)).size() > 0){
           throw new CustomException("You have already rated this card!", HttpStatus.FORBIDDEN);
       }
       rating.setUser(user);
       rating.setCard(load(cardId));
       Card card = cardRepository.findById(cardId).get();
       card.addRating(rating);
       cardRepository.save(card);
       return card.getRatings();
    }

    public void removeCard(long cardId) {
        Card card = cardRepository.findById(cardId).get();
        cardRepository.delete(card);
    }
}
