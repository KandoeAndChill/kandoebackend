package be.kdg.kandoebackend.service;

import be.kdg.kandoebackend.error.CustomException;
import be.kdg.kandoebackend.model.CardSession;
import be.kdg.kandoebackend.model.Session;
import be.kdg.kandoebackend.model.Snapshot;
import be.kdg.kandoebackend.model.SnapshotTurn;
import be.kdg.kandoebackend.persistence.SessionRepository;
import be.kdg.kandoebackend.persistence.SnapshotRepository;
import be.kdg.kandoebackend.persistence.SnapshotTurnRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;

@Service
@Transactional
public class SnapshotService {

    private final SnapshotRepository snapshotRepository;
    private final SessionRepository sessionRepository;
    private final SessionService sessionService;
    private final CardSessionService cardSessionService;
    private final SnapshotTurnRepository snapshotTurnRepository;

    @Autowired
    public SnapshotService(SnapshotRepository snapshotRepository, SessionRepository sessionRepository, SessionService sessionService, CardSessionService cardSessionService, SnapshotTurnRepository snapshotTurnRepository) {
        this.snapshotRepository = snapshotRepository;
        this.sessionRepository = sessionRepository;
        this.sessionService = sessionService;
        this.cardSessionService = cardSessionService;
        this.snapshotTurnRepository = snapshotTurnRepository;
    }

    public Snapshot save(Snapshot snapshot) {
        return snapshotRepository.save(snapshot);
    }

    public Snapshot loadSnapshot(Long id) throws CustomException {
        Optional<Snapshot> optionalSnapshot = snapshotRepository.findById(id);
        if (optionalSnapshot.isPresent()) {
            return optionalSnapshot.get();
        }
        throw new CustomException("Snapshot not found!", HttpStatus.NOT_FOUND);
    }

    public SnapshotTurn loadSnapshotTurn(Long id) throws CustomException {
        Optional<SnapshotTurn> optionalSnapshotTurn = snapshotTurnRepository.findById(id);
        if (optionalSnapshotTurn.isPresent()) {
            return optionalSnapshotTurn.get();
        }
        throw new CustomException("SnapshotTurn not found!", HttpStatus.NOT_FOUND);
    }

    public List<Snapshot> getSnapshotBySession(Long id) throws CustomException {
        Optional<Session> optionalSession = sessionRepository.findById(id);
        if (optionalSession.isPresent()){
            Session session = optionalSession.get();
            return snapshotRepository.findAllBySession(session);
        }
        throw new CustomException("No Session found", HttpStatus.NOT_FOUND);
    }

    public List<SnapshotTurn> getAllSnapshotTurnsBySnapshot(Long id) throws CustomException {
        Optional<Snapshot> optionalSnapshot = snapshotRepository.findById(id);
        if (optionalSnapshot.isPresent()){
            Snapshot snapshot = optionalSnapshot.get();
            return snapshotTurnRepository.findAllBySnapshot(snapshot);
        }
        throw new CustomException("No Snapshot found", HttpStatus.NOT_FOUND);
    }

    public Snapshot createSnapshotForSession(Long sessionId){
        Session session = sessionService.load(sessionId);
        Snapshot snapshot = new Snapshot();
        snapshot.setSession(session);
        snapshot.setSnapshotDate(LocalDateTime.now());
        snapshot.setNumberOfRounds(session.getNumberOfRounds());
        return save(snapshot);
    }

    public SnapshotTurn createSnapshotTurnForSession(Long sessionId, Long cardId, boolean isAutomaticSnapshot){
        Session session = sessionService.load(sessionId);
        SnapshotTurn snapshotTurn = new SnapshotTurn();
        snapshotTurn.setActiveUser(session.getActiveUser());
        snapshotTurn.setCurrentRound(session.getCurrentRound());
        snapshotTurn.setSelectedCard(cardSessionService.load(cardId));
        snapshotTurn.setAutomaticShot(isAutomaticSnapshot);
        return snapshotTurnRepository.save(snapshotTurn);
    }
}
