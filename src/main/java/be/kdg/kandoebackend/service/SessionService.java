package be.kdg.kandoebackend.service;

import be.kdg.kandoebackend.error.CustomException;
import be.kdg.kandoebackend.error.MinAmountOfCardsException;
import be.kdg.kandoebackend.model.*;
import be.kdg.kandoebackend.persistence.CardSessionRepository;
import be.kdg.kandoebackend.persistence.SessionRepository;
import be.kdg.kandoebackend.persistence.ThemeRepository;
import be.kdg.kandoebackend.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class SessionService {

    private final SessionRepository sessionRepository;
    private final ThemeRepository themeRepository;
    private final UserRepository userRepository;
    private final CardSessionRepository cardSessionRepository;
    private final CardSessionService cardSessionService;

    @Autowired
    public SessionService(SessionRepository sessionRepository, ThemeRepository themeRepository, UserRepository userRepository, CardSessionRepository cardSessionRepository, CardSessionService cardSessionService) {
        this.sessionRepository = sessionRepository;
        this.themeRepository = themeRepository;
        this.userRepository = userRepository;
        this.cardSessionRepository = cardSessionRepository;
        this.cardSessionService = cardSessionService;
    }

    public Session createSession(Session session) {
        Session createdSession = sessionRepository.save(session);
        if (session.getStartDate().isEqual(LocalDateTime.now()) || session.getStartDate().isBefore(LocalDateTime.now())) {
            startSession(createdSession.getId());
        }
        return createdSession;
    }

    public Session save(Session session){
        return sessionRepository.save(session);
    }

    public Session load(Long id) throws CustomException {
        Optional<Session> optionalSession = sessionRepository.findById(id);
        if(optionalSession.isPresent()){
            return optionalSession.get();
        }
        throw new CustomException("Session not found", HttpStatus.NOT_FOUND);
    }

    public void deleteSession(Long sessionId) throws CustomException {
        Optional<Session> optionalSession = sessionRepository.findById(sessionId);
        if (optionalSession.isPresent()){
            sessionRepository.deleteById(sessionId);
        }
        else {
            throw new CustomException("Session not found", HttpStatus.NOT_FOUND);
        }
    }

    public Session cloneSession(Long sessionId, User organiser){
        Session session = load(sessionId);
        Session clonedSession = new Session();
        clonedSession.setName(session.getName());
        clonedSession.setTheme(session.getTheme());
        clonedSession.setNumberOfCircles(session.getNumberOfCircles());
        clonedSession.setMinCards(session.getMinCards());
        clonedSession.setMaxCards(session.getMaxCards());
        clonedSession.setChanceCircle(session.isChanceCircle());
        clonedSession.setUserCardsEnabled(session.isUserCardsEnabled());
        clonedSession.setOrganiser(organiser);
        clonedSession.setStartDate(LocalDateTime.now());
        clonedSession.setNumberOfRounds(session.getNumberOfRounds());
        clonedSession.setRoundTimeInMinutes(session.getRoundTimeInMinutes());
        return createSession(clonedSession);
    }

    public List<Session> getAllSessionsByTheme(Long id) throws CustomException {
        Optional<Theme> optionalTheme = themeRepository.findById(id);
        if(optionalTheme.isPresent()){
            Theme theme = optionalTheme.get();
            return sessionRepository.findAllByTheme(theme);
        }
        throw new CustomException("Theme not found", HttpStatus.NOT_FOUND);
    }

    public List<Session> getAllPassedSessionsByTheme(Long id) throws CustomException {
        Optional<Theme> optionalTheme = themeRepository.findById(id);
        if(optionalTheme.isPresent()){
            Theme theme = optionalTheme.get();
            return sessionRepository.findAllByThemeAndGameOver(theme, true);
        }
        throw new CustomException("Theme not found for passed sessions!", HttpStatus.NOT_FOUND);
    }

    public List<Session> getAllActiveSessionsByTheme(Long id) throws CustomException {
        Optional<Theme> optionalTheme = themeRepository.findById(id);
        if(optionalTheme.isPresent()){
            Theme theme = optionalTheme.get();
            return sessionRepository.findAllByThemeAndGameOverAndStartDateBefore(theme, false, LocalDateTime.now());
        }
        throw new CustomException("Theme not found for active sessions!", HttpStatus.NOT_FOUND);
    }

    public List<Session> getAllPlannedSessionsByTheme(Long id) throws CustomException {
        Optional<Theme> optionalTheme = themeRepository.findById(id);
        if(optionalTheme.isPresent()){
            Theme theme = optionalTheme.get();
            return sessionRepository.findAllByThemeAndGameOverAndStartDateAfter(theme, false, LocalDateTime.now());
        }
        throw new CustomException("Theme not found for planned sessions!", HttpStatus.NOT_FOUND);
    }

    public String checkState(Long id) throws CustomException {
        Optional<Session> optionalSession = sessionRepository.findById(id);
        if(optionalSession.isPresent()){
            Session session = optionalSession.get();
            if(session.isGameOver()){
                return "Passed";
            }
            else if(session.getStartDate().isAfter(LocalDateTime.now())){
                return "Planned";
            }
            else if(session.getStartDate().isBefore(LocalDateTime.now()) && !session.isGameOver()){
                return "Active";
            }
        }
        throw new CustomException("Session not Found", HttpStatus.NOT_FOUND);
    }

    public List<Session> getAllSessionsByOrganiser(Long id) throws CustomException {
        Optional<User> optionalUser = userRepository.findById(id);
        if(optionalUser.isPresent()){
            User user = optionalUser.get();
            return sessionRepository.findAllByOrganiser(user);
        }
        throw new CustomException("User not found", HttpStatus.NOT_FOUND);
    }

    public List<Session> getAllSessionsByUser(Long id) throws CustomException {
        Optional<User> optionalUser = userRepository.findById(id);
        if(optionalUser.isPresent()){
            User user = optionalUser.get();
            return sessionRepository.findAllByUserListContaining(user);
        }
        throw new CustomException("User not found", HttpStatus.NOT_FOUND);
    }

    public List<Session> getAllActiveSessionsOfUser(Long id) throws CustomException{
        Optional<User> optionalUser = userRepository.findById(id);
        if(optionalUser.isPresent()){
            User user = optionalUser.get();
            return sessionRepository.findAllByUserListContainingAndGameOverAndStartDateBefore(user, false, LocalDateTime.now());
        }
        throw new CustomException("User not found for sessions", HttpStatus.NOT_FOUND);
    }

    public List<Session> getAllPlannedSessionsOfUser(Long id) throws CustomException{
        Optional<User> optionalUser = userRepository.findById(id);
        if(optionalUser.isPresent()){
            User user = optionalUser.get();
            return sessionRepository.findAllByUserListContainingAndGameOverAndStartDateAfter(user, false, LocalDateTime.now());
        }
        throw new CustomException("User not found for sessions", HttpStatus.NOT_FOUND);
    }

    public List<Session> getAllPassedSessionsOfUser(Long id) throws CustomException{
        Optional<User> optionalUser = userRepository.findById(id);
        if(optionalUser.isPresent()){
            User user = optionalUser.get();
            return sessionRepository.findAllByUserListContainingAndGameOver(user, true);
        }
        throw new CustomException("User not found for sessions", HttpStatus.NOT_FOUND);
    }

    private boolean checkEndSession(Long id) throws CustomException {
        Optional<Session> optionalSession = sessionRepository.findById(id);
        if(optionalSession.isPresent()){
            Session session = optionalSession.get();
            List<CardSession> cardSessionList = cardSessionRepository.findAllBySession(session);
            int maxPriority = (cardSessionList.stream().mapToInt(CardSession::getPriority).max()).getAsInt();
            if (session.getNumberOfRounds() == session.getCurrentRound()){
                session.setNotificationMessage("Max number of rounds reached");
                return true;
            }
            else if(session.getNumberOfCircles() == maxPriority){
                session.setNotificationMessage("Max priority reached");
                return true;
            }
            else{
                return false;
            }
        } throw new CustomException("No session found to check", HttpStatus.NOT_FOUND);
    }

    public Session endSession(Long id) throws CustomException {
        Optional<Session> optionalSession = sessionRepository.findById(id);
        if(optionalSession.isPresent()){
            Session session = optionalSession.get();
            session.setGameOver(true);
            return sessionRepository.save(session);
        }
        throw new CustomException("Session not found, can not end!", HttpStatus.NOT_FOUND);
    }

    public Session startSession(Long sessionId) throws CustomException {
        Optional<Session> optionalSession = sessionRepository.findById(sessionId);
        if(optionalSession.isPresent()){
            Session session = optionalSession.get();
            session.setActiveUser(session.getOrganiser());
            return sessionRepository.save(session);
        }
        throw new CustomException("Session not found, can not start session!", HttpStatus.NOT_FOUND);
    }

    private boolean isUserTurn(Long sessionId, User currentUser) throws CustomException {
        Session session = load(sessionId);
        User activeUser = session.getActiveUser();
        return currentUser == activeUser;
    }

    private Session cardSelected(Long cardSessionId) throws CustomException {
        CardSession cardSession = cardSessionService.incrementPriority(cardSessionId);
        List<User> userList = cardSession.getSession().getUserList();
        User activeUser = cardSession.getSession().getActiveUser();
        User newActiveUser;
        int position = userList.indexOf(activeUser);
        if (position == userList.size()-1){
            newActiveUser = userList.get(0);
        }
        else {
            newActiveUser = userList.get(position + 1);
        }
        if (newActiveUser == cardSession.getSession().getOrganiser()) {
            cardSession.getSession().setCurrentRound(cardSession.getSession().getCurrentRound() + 1);
        }
        cardSession.getSession().setActiveUser(newActiveUser);
        return sessionRepository.save(cardSession.getSession());
    }

    public Session changeUserTurn(Long cardSessionId) throws CustomException {
        CardSession cardSession = cardSessionService.load(cardSessionId);
        List<User> userList = cardSession.getSession().getUserList();
        User activeUser = cardSession.getSession().getActiveUser();
        User newActiveUser;
        int position = userList.indexOf(activeUser);
        if (position == userList.size()-1){
            newActiveUser = userList.get(0);
        }
        else {
            newActiveUser = userList.get(position + 1);
        }
        if (newActiveUser == cardSession.getSession().getOrganiser()) {
            cardSession.getSession().setCurrentRound(cardSession.getSession().getCurrentRound() + 1);
        }
        cardSession.getSession().setActiveUser(newActiveUser);
        return sessionRepository.save(cardSession.getSession());
    }

    public boolean isSessionParticipant(User user, long sessionId) {
        List<Session> userSessionList =  getAllSessionsByUser(user.getId());
        return userSessionList.stream().filter(session -> session.getId() == sessionId).collect(Collectors.toList()).size() > 0;
    }

    public boolean isSessionOrganiser(User user, long sessionId){
        List<Session> userSessionList =  getAllSessionsByOrganiser(user.getId());
        return userSessionList.stream().filter(session -> session.getId() == sessionId).collect(Collectors.toList()).size() > 0;
    }

    public Session takeTurnWithChecks(Long cardSessionId, Session session, User user){
        if(!session.isGameOver()){
            if(isUserTurn(session.getId(), user)) {
                cardSelected(cardSessionId);
            }
            else { throw new CustomException("It is not your turn!", HttpStatus.FORBIDDEN);}
        }
        else{ throw new CustomException("The Game is Over: " + session.getNotificationMessage(), HttpStatus.FORBIDDEN);}

        if(checkEndSession(session.getId())){
            session = endSession(session.getId());
        }
        return session;
    }

    @Transactional
    public Session cardSelectionChange(Card card, Long sessionId, User user) throws CustomException {
        Session session = load(sessionId);
        if (session.isCardsSelected()) {
            throw new CustomException("Cards have already been selected for this session, this phase is over...!", HttpStatus.BAD_REQUEST);
        }
        if (!isUserTurn(session.getId(), user)) {
            throw new CustomException("Please wait for your turn!", HttpStatus.FORBIDDEN);
        }
        if (checkMaxCards(session)) {
            throw new CustomException("Each user can select a max. of  " + session.getMaxCards() + "amount of cards! Waiting for organiser to start session...", HttpStatus.BAD_REQUEST);
        }
        session = cardSelectedChangeTurn(session, user);
        this.cardSessionService.addCardToSession(card, session);
        return save(session);
    }

    private boolean checkMaxCards(Session session) {
        return (session.getCardList().size() + 1) > (session.getMaxCards() * session.getUserList().size());
    }

    private List<User> getUserListOrdenedByRole(List<User> userList, long sessionId) {
        List<User> allUsersOrdenedByRole = new ArrayList<User>();
        //Can be 1 or more. Sort organizers by username.
        List<User> organisers = userList.stream().filter(user -> isSessionOrganiser(user, sessionId)).sorted(Comparator.comparing(User::getUsername)).collect(Collectors.toList());
        allUsersOrdenedByRole.addAll(organisers);
        List<User> participants = userList.stream().filter(user -> !isSessionOrganiser(user, sessionId)).collect(Collectors.toList());
        allUsersOrdenedByRole.addAll(participants);
        return allUsersOrdenedByRole;
        
    }

    private boolean checkMinCardsSession(Session session) {
        return session.getCardList().size() >= (session.getMinCards() * session.getUserList().size());
    }

    private void setNextRound(Session session) {
        if (session.getCurrentRound() < session.getNumberOfRounds()) {
            int currentRound = session.getCurrentRound();
            session.setCurrentRound(currentRound++);
        } else {
            endSession(session.getId());
        }
    }

    // Can be used seperately to skip a turn...
    @Transactional
    public Session cardSelectedChangeTurn(Session sessionOfCard, User user) throws CustomException {
        if (sessionOfCard.isCardsSelected()) {
            throw new CustomException("Cards have already been selected for this session, this phase is over...!", HttpStatus.BAD_REQUEST);
        }
        if (!isUserTurn(sessionOfCard.getId(), user)) {
            throw new CustomException("Please wait for your turn!", HttpStatus.FORBIDDEN);
        }
        List<User> userList = getUserListOrdenedByRole(sessionOfCard.getUserList(), sessionOfCard.getId());
        User activeUser = sessionOfCard.getActiveUser();
        User newActiveUser;
        int position = userList.indexOf(activeUser);
        if (position == userList.size() - 1) {
            newActiveUser = userList.get(0);
        } else {
            newActiveUser = userList.get(position + 1);
        }
        sessionOfCard.setActiveUser(newActiveUser);
        return save(sessionOfCard);
    }

    public List<Session> getAllActiveSessionsWithSelectedCardsOfUser(Long id) throws CustomException{
        Optional<User> optionalUser = userRepository.findById(id);
        if(optionalUser.isPresent()){
            User user = optionalUser.get();
            return sessionRepository.findAllByUserListContainingAndGameOverAndStartDateBeforeAndCardsSelected(user, false, LocalDateTime.now(), true);
        }
        throw new CustomException("User not found for sessions", HttpStatus.NOT_FOUND);
    }

    public List<Session> getAllSessionsBetweenNowAndAMinute(){
        List<Session> sessionList = sessionRepository.findAllByGameOverAndStartDateBetween(false, LocalDateTime.now(), LocalDateTime.now().plusSeconds(59));
        return sessionList;
    }

    // Manually fired by organiser of session...
    @Transactional
    public Session endSelectionPhase(Session session, User user) {

        if (session.isCardsSelected()) {
            throw new CustomException("Cards have already been selected for this session, this phase is over...!", HttpStatus.BAD_REQUEST);
        }
        if (!checkMinCardsSession(session)) {
            cardSelectedChangeTurn(session, user);
            //Custom HTTP status given in GlobalExceptionHandler ( @RestControllerAdvice ) because particular action (refresh of session) is needed by Front-End
            throw new MinAmountOfCardsException("Each user has to select at least " + session.getMinCards() + "card(s)! A total amount of " + session.getMinCards() * session.getUserList().size() + " is needed!");
        }
        session.setCardsSelected(true);
        setNextRound(session);
        return save(session);
    }
}
