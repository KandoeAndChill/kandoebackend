package be.kdg.kandoebackend.service;

import be.kdg.kandoebackend.error.CustomException;
import be.kdg.kandoebackend.model.ChatMessage;
import be.kdg.kandoebackend.model.Session;
import be.kdg.kandoebackend.persistence.ChatMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ChatMessageService {
    private final ChatMessageRepository chatMessageRepository;
    private final SessionService sessionService;

    @Autowired
    public ChatMessageService(ChatMessageRepository chatMessageRepository, SessionService sessionService) {
        this.chatMessageRepository = chatMessageRepository;
        this.sessionService = sessionService;
    }

    public ChatMessage save(ChatMessage card) {
        return chatMessageRepository.save(card);
    }

    public ChatMessage load(Long id) throws CustomException {
        Optional<ChatMessage> optionalChatMessage = chatMessageRepository.findById(id);
        if (optionalChatMessage.isPresent()) {
            return optionalChatMessage.get();
        }
        throw new CustomException("Chat not found!", HttpStatus.NOT_FOUND);
    }

    public List<ChatMessage> loadChatForSession(Long id) throws CustomException{
        Session session = sessionService.load(id);
        return chatMessageRepository.findAllBySession(session);
    }
}
