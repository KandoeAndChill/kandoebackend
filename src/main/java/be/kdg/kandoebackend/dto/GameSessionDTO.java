package be.kdg.kandoebackend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GameSessionDTO {
    @Id
    private Long id;
    @NotEmpty
    private String name;
    private boolean chanceCircle;
    private Long themeId;
    private Long organiserId;
    private boolean gameOver;
    private boolean userCardsEnabled;
    private boolean cardsSelected;
    private Long activeUserId;
    private List<CardSessionDTO> cardList;
    private int numberOfRounds;
    private int currentRound;
    private String notificationMessage;
    private int roundTimeInMinutes;
    private int numberOfCircles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getThemeId() {
        return themeId;
    }

    public void setThemeId(Long themeId) {
        this.themeId = themeId;
    }

    public Long getOrganiserId() {
        return organiserId;
    }

    public void setOrganiserId(Long organiserId) {
        this.organiserId = organiserId;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public Long getActiveUserId() {
        return activeUserId;
    }

    public void setActiveUserId(Long activeUserId) {
        this.activeUserId = activeUserId;
    }

    public List<CardSessionDTO> getCardList() {
        return cardList;
    }

    public void setCardList(List<CardSessionDTO> cardList) {
        this.cardList = cardList;
    }

    public int getNumberOfRounds() {
        return numberOfRounds;
    }

    public void setNumberOfRounds(int numberOfRounds) {
        this.numberOfRounds = numberOfRounds;
    }

    public int getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(int currentRound) {
        this.currentRound = currentRound;
    }

    public int getRoundTimeInMinutes() {
        return roundTimeInMinutes;
    }

    public void setRoundTimeInMinutes(int roundTimeInMinutes) {
        this.roundTimeInMinutes = roundTimeInMinutes;
    }
}
