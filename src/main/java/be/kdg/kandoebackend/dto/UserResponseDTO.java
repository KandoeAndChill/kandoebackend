package be.kdg.kandoebackend.dto;

import be.kdg.kandoebackend.model.Role;

import java.util.List;

// Used when querying current authenticated user
public class UserResponseDTO {

    private String username;
    private String email;
    private List<Role> roles;
    private String avatarBase64;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getAvatarBase64() {
        return avatarBase64;
    }

    public void setAvatarBase64(String avatarBase64) {
        this.avatarBase64 = avatarBase64;
    }

}
