package be.kdg.kandoebackend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SnapshotDTO {

    @Id
    private Long id;
    private Long sessionId;
    private LocalDateTime snapshotDate;
    private List<SnapshotTurnDTO> snapshotTurnDTOList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public LocalDateTime getDate() {
        return snapshotDate;
    }

    public void setDate(LocalDateTime date) {
        this.snapshotDate = date;
    }
}
