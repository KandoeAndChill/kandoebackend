package be.kdg.kandoebackend.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SessionDTO {
    @Id
    private Long id;
    @NotEmpty
    private String name;
    private boolean chanceCircle;
    private int numberOfCircles;
    private int minCards;
    private int maxCards;
    private Long themeId;
    private LocalDateTime startTime;
    @JsonIgnore
    private Long organiserId;
    private boolean userCardsEnabled;
    private int numberOfRounds;
    private int roundTimeInMinutes;
    private String state;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChanceCircle() {
        return chanceCircle;
    }

    public void setChanceCircle(boolean chanceCircle) {
        this.chanceCircle = chanceCircle;
    }

    public int getNumberOfCircles() {
        return numberOfCircles;
    }

    public void setNumberOfCircles(int numberOfCircles) {
        this.numberOfCircles = numberOfCircles;
    }

    public int getMinCards() {
        return minCards;
    }

    public void setMinCards(int minCards) {
        this.minCards = minCards;
    }

    public int getMaxCards() {
        return maxCards;
    }

    public void setMaxCards(int maxCards) {
        this.maxCards = maxCards;
    }

    public boolean isUserCardsEnabled() {
        return userCardsEnabled;
    }

    public void setUserCardsEnabled(boolean userCardsEnabled) {
        this.userCardsEnabled = userCardsEnabled;
    }

    public int getNumberOfRounds() {
        return numberOfRounds;
    }

    public void setNumberOfRounds(int numberOfRounds) {
        this.numberOfRounds = numberOfRounds;
    }

    public int getRoundTimeInMinutes() {
        return roundTimeInMinutes;
    }

    public void setRoundTimeInMinutes(int roundTimeInMinutes) {
        this.roundTimeInMinutes = roundTimeInMinutes;
    }

    public Long getThemeId() {
        return themeId;
    }

    public void setThemeId(Long themeId) {
        this.themeId = themeId;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public Long getOrganiserId() {
        return organiserId;
    }

    public void setOrganiserId(Long organiserId) {
        this.organiserId = organiserId;
    }

    public void setState(String state) {
        this.state = state;
    }
}
