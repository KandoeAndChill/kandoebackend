package be.kdg.kandoebackend.dto;

// Used as a 'wrapper' for incoming Login form data credentials.
public class UserLoginDTO {

    //can  be either email address or username.
    private String userIdentifier;
    private String password;

    public String getUserIdentifier() {
        return userIdentifier;
    }
    public void setUserIdentifier(String userIdentifier) {
        this.userIdentifier = userIdentifier;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }


}
