package be.kdg.kandoebackend.dto;

import lombok.Data;

@Data
public class RatingResponseDTO {

    private String comment;
    private String username;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
