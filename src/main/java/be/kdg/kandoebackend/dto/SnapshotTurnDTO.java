package be.kdg.kandoebackend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SnapshotTurnDTO {

    @Id
    private Long id;
    private Long snapshotId;
    private CardSessionDTO selectedCard;
    private Long activeUserId;
    private int currentRound;
    private boolean automaticShot;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSnapshotId() {
        return snapshotId;
    }

    public void setSnapshotId(Long snapshotId) {
        this.snapshotId = snapshotId;
    }

    public CardSessionDTO getSelectedCard() {
        return selectedCard;
    }

    public void setSelectedCard(CardSessionDTO selectedCard) {
        this.selectedCard = selectedCard;
    }

    public Long getActiveUserId() {
        return activeUserId;
    }

    public void setActiveUserId(Long activeUserId) {
        this.activeUserId = activeUserId;
    }

    public int getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(int currentRound) {
        this.currentRound = currentRound;
    }

    public boolean isAutomaticShot() {
        return automaticShot;
    }

    public void setAutomaticShot(boolean automaticShot) {
        this.automaticShot = automaticShot;
    }
}
