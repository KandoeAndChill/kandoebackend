package be.kdg.kandoebackend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardDTO {

    private Long id;
    @NotEmpty
    @Size(min = 4, max = 20, message = "Card must contain word(s) and can be max size of 20!")
    private String name;
    private String imageBase64Url;
    private Long themeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageBase64Url() {
        return imageBase64Url;
    }

    public void setImageBase64Url(String imgUrl) {
        this.imageBase64Url = imgUrl;
    }

    public Long getThemeId() {
        return themeId;
    }

    public void setThemeId(Long themeId) {
        this.themeId = themeId;
    }

}
