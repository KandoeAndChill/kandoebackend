package be.kdg.kandoebackend.dto;

import java.util.List;

public class UserInviteDTO {

    private long sessionId;
    private List<String> mailList;

    public long getSessionId() {
        return sessionId;
    }

    public void setSessionId(long sessionId) {
        this.sessionId = sessionId;
    }

    public List<String> getMailList() {
        return mailList;
    }

    public void setMailList(List<String> mailList) {
        this.mailList = mailList;
    }
}
