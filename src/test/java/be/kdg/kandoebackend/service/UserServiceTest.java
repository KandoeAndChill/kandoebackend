package be.kdg.kandoebackend.service;

import be.kdg.kandoebackend.model.Role;
import be.kdg.kandoebackend.model.User;
import be.kdg.kandoebackend.security.JwtTokenProvider;
import org.checkerframework.checker.units.qual.A;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserServiceTest {
    @Autowired
    private UserService userService;
    private User user;
    JwtTokenProvider jwtTokenProvider;

    @Before
    public void setup(){
        user = new User();
        user.setEmail("string@123.be");
        user.setPassword("string123456");
        user.setUsername("FirstUser");
    }

    @Test
    public void testSave(){
        String savedUser = userService.save(user);
        assertNotNull("Token should be generated", savedUser);
    }

    @Test
    public void testLogin(){
        String savedUser = userService.save(user);
        String loggedInUser = userService.signin("FirstUser", "string123456");
        assertNotNull(loggedInUser);
    }

    @Test
    public void testFindUserByEmail(){
        String savedUser = userService.save(user);
        User testUser = userService.findUserByEmail(user.getEmail());
        assertNotNull(testUser);
    }

    @Test
    public void testIsMailRegisteredSuccess(){
        String savedUser = userService.save(user);
        assertTrue(userService.isMailRegistered(user.getEmail()));
    }

    @Test
    public void testIsMailRegisteredFail(){
        String savedUser = userService.save(user);
        assertFalse(userService.isMailRegistered("random@123.be"));
    }
}