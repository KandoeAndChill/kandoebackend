package be.kdg.kandoebackend.service;

import be.kdg.kandoebackend.model.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class CardSessionServiceTest {
    @Autowired
    private CardSessionService cardSessionService;
    @Autowired
    private SessionService sessionService;
    @Autowired
    private ThemeService themeService;
    @Autowired
    private CardService cardService;
    @Autowired
    private UserService userService;

    private CardSession cardSession;
    private Session session;
    private Theme theme;
    private User user;

    @Before
    public void setupTests(){
        user = new User();
        user.setUsername("FirstUser");
        user.setPassword("password123");
        user.setEmail("test@123.be");
        userService.save(user);

        theme = new Theme();
        theme.setName("Name");
        theme.setDescription("Deekfneaopfjejfie");
        List<Tag> tagList = new ArrayList<>();
        theme.setTagList(tagList);
        theme.setUser(user);
        themeService.save(theme);

        session = new Session();
        session.setName("sessionName");
        session.setChanceCircle(true);
        session.setMinCards(1);
        session.setMaxCards(3);
        session.setNumberOfCircles(5);
        session.setTheme(theme);
        session.setNumberOfRounds(5);
        session.setRoundTimeInMinutes(1);
        session.setStartDate(LocalDateTime.now());
        session.setOrganiser(user);
        sessionService.save(session);

        cardSession = new CardSession();
        cardSession.setCardText("cardSession");
        cardSession.setImageUrl("link");
        cardSession.setSession(session);
        cardSessionService.save(cardSession);
    }

    @Test
    public void testCreateSession(){
        CardSession loadedCardSession = cardSessionService.load(1L);
        Assert.assertEquals(cardSession, loadedCardSession);
    }

    @Test
    public void testGetAllCardSessionsBySession() {
        CardSession cardSession2 = new CardSession();
        cardSession2.setCardText("cardSession2");
        cardSession2.setImageUrl("link2");
        cardSession2.setSession(session);
        cardSessionService.save(cardSession2);
        Assert.assertEquals(2, cardSessionService.getCardSessionsForSession(1L).size());
    }

    @Test
    public void testIncrementPriority(){
        CardSession editedCardSession = cardSessionService.incrementPriority(1L);
        Assert.assertEquals((cardSession.getPriority() + 1), editedCardSession.getPriority());
    }

    @Test
    public void testAddCardToSession(){
        Card card = new Card();
        card.setId(1L);
        card.setName("TestCard");
        card.setImageBase64Url("TestLink");
        card.setTheme(theme);
        cardService.save(card);

        CardSession cardToCardSession = cardSessionService.addCardToSession(card, session);
        Assert.assertEquals(card.getName(), cardToCardSession.getCardText());
    }
}
