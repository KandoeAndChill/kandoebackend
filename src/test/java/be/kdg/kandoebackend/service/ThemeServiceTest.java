package be.kdg.kandoebackend.service;

import be.kdg.kandoebackend.error.CustomException;
import be.kdg.kandoebackend.model.Tag;
import be.kdg.kandoebackend.model.Theme;
import be.kdg.kandoebackend.model.User;
import be.kdg.kandoebackend.persistence.ThemeRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ThemeServiceTest {

    @Autowired
    ThemeService themeService;
    @Autowired
    ThemeRepository themeRepository;
    @Autowired
    UserService userService;

    private Theme theme;
    private User user;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void makeThemeForTest() throws CustomException {
        user = new User();
        user.setEmail("string@123.be");
        user.setPassword("string123456");
        user.setUsername("FirstUser");
        userService.save(user);
        user = userService.findUserByEmail("string@123.be");

        theme = new Theme();
        theme.setId(1L);
        theme.setName("testName");
        theme.setDescription("testDescription");
        List<Tag> tagList = new ArrayList<>();
        theme.setTagList(tagList);
        theme.setCommentaryAllowed(false);
    }

    @Test
    public void testThemeService() throws CustomException {
        Theme savedTheme = themeService.save(theme);
        Theme loadedTheme = themeService.load(1L);
        Assert.assertEquals(savedTheme, loadedTheme);
        Assert.assertEquals("Name should be the same", theme.getName(), savedTheme.getName());
        Assert.assertEquals("Description should be the same", theme.getDescription(), savedTheme.getDescription());
        Assert.assertEquals("CommentaryAllowed should be the same", theme.isCommentaryAllowed(), savedTheme.isCommentaryAllowed());
        //Assert.assertEquals("User must be the same", user.getUsername(), savedTheme.getUser().getUsername());
    }

    //Improvement: make a custom exception
    @Test(expected = InvalidDataAccessApiUsageException.class)
    public void testAddingEmptyTheme(){
        Theme theme = null;
        themeService.save(theme);
    }

    //Improvement: add check and exception to themename
    @Test(expected = ConstraintViolationException.class)
    public void testAddingEmptyNameTheme(){
        theme.setName(null);
        themeService.save(theme);
    }

    @Test
    public void find() {
        Theme savedTheme = themeService.save(theme);
        Assert.assertNotNull(savedTheme);

        Theme loadedTheme = themeService.load(savedTheme.getId());
        Assert.assertEquals(savedTheme.getName(), loadedTheme.getName());
    }

    @Test
    public void delete() {
        thrown.expect(CustomException.class);
        thrown.expectMessage(is("Theme not found!"));
        Theme savedTheme = themeService.save(theme);
        themeService.delete(1L);
        themeService.load(1L);
    }

    @Test
    public void updateThemeName() {
        themeService.updateThemeName(theme, "newName2");
        Assert.assertEquals("newName2", theme.getName());

    }

    @Test
    public void updateThemeDescription() {
        themeService.updateThemeDescription(theme, "NewDescription2");
        Assert.assertEquals("NewDescription2", theme.getDescription());
    }

    @Test
    public void updateThemeCommentaryAllowed() {
        themeService.updateThemeCommentaryAllowed(theme, true);
        Assert.assertTrue(theme.isCommentaryAllowed());
    }

    @Test
    public void getAllThemes() {
        Theme theme2 = new Theme();
        Theme theme3 = new Theme();
        theme2.setId(2L);
        theme2.setName("testName2");
        theme2.setDescription("testDescription2");
        theme3.setId(3L);
        theme3.setName("testName3");
        theme3.setDescription("testDescription3");
        Theme savedTheme = themeService.save(theme);
        Theme savedTheme2 = themeService.save(theme2);
        Theme savedTheme3 = themeService.save(theme3);
        Assert.assertEquals(3, themeRepository.count());
    }
}