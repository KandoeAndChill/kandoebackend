package be.kdg.kandoebackend.service;

import be.kdg.kandoebackend.error.CustomException;
import be.kdg.kandoebackend.model.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Transactional
public class SessionServiceTest {
    @Autowired
    private SessionService sessionService;
    @Autowired
    private ThemeService themeService;
    @Autowired
    private UserService userService;
    @Autowired
    private CardSessionService cardSessionService;

    private List<CardSession> cardSessions = new ArrayList<>();
    private Session session;
    private User user;
    private User user2;

    @Before
    public void setupTests(){
        user = new User();
        user.setUsername("FirstUser");
        user.setPassword("password123");
        user.setEmail("test@123.be");
        userService.save(user);
        // loggedInUser = userService.signin("FirstUser", "password123");

        user2 = new User();
        user2.setUsername("SecondUser");
        user2.setPassword("password124");
        user2.setEmail("test@124.be");
        userService.save(user2);

        List<User> userList = new ArrayList<>();
        userList.add(user);
        userList.add(user2);

        Theme theme = new Theme();
        theme.setName("Name");
        theme.setDescription("testtesttest");
        List<Tag> tagList = new ArrayList<>();
        theme.setTagList(tagList);
        theme.setUser(user);
        Theme savedTheme = themeService.save(theme);

        session = new Session();
        session.setName("sessionName");
        session.setChanceCircle(true);
        session.setMinCards(1);
        session.setMaxCards(3);
        session.setNumberOfCircles(3);
        session.setTheme(savedTheme);
        session.setNumberOfRounds(2);
        session.setRoundTimeInMinutes(1);
        session.setStartDate(LocalDateTime.now());
        session.setOrganiser(user);
        session.setUserList(userList);
        sessionService.save(session);

        Session session2 = new Session();
        session2.setName("sessionName2");
        session2.setChanceCircle(true);
        session2.setMinCards(1);
        session2.setMaxCards(3);
        session2.setNumberOfCircles(5);
        session2.setTheme(savedTheme);
        session2.setNumberOfRounds(5);
        session2.setRoundTimeInMinutes(1);
        session2.setStartDate(LocalDateTime.of(2420, Month.JANUARY, 1, 13, 37, 42));
        session2.setOrganiser(user);
        session2.setUserList(userList);
        sessionService.save(session2);

        Session session3 = new Session();
        session3.setName("sessionName3");
        session3.setChanceCircle(true);
        session3.setMinCards(1);
        session3.setMaxCards(3);
        session3.setNumberOfCircles(5);
        session3.setTheme(savedTheme);
        session3.setNumberOfRounds(5);
        session3.setRoundTimeInMinutes(1);
        session3.setStartDate(LocalDateTime.of(2000, Month.JANUARY, 1, 13, 37, 42));
        session3.setOrganiser(user);
        session3.setGameOver(true);
        session3.setUserList(userList);
        sessionService.save(session3);

        CardSession cardSession = new CardSession();
        cardSession.setCardText("test1");
        cardSession.setImageUrl("link");
        cardSession.setSnapshotPriority(0);
        cardSessions.add(cardSession);

        CardSession cardSession2 = new CardSession();
        cardSession2.setCardText("test2");
        cardSession2.setImageUrl("link");
        cardSession2.setSnapshotPriority(0);
        cardSessions.add(cardSession2);

    }

    @Test
    public void getAllSessionsByTheme() {
        List<Session> sessionsByTheme = sessionService.getAllSessionsByTheme(1L);
        Assert.assertEquals(3, sessionsByTheme.size());
    }

    @Test
    public void getAllPassedSessionsByTheme() {
        List<Session> sessionsByTheme = sessionService.getAllPassedSessionsByTheme(1L);
        Assert.assertEquals(1, sessionsByTheme.size());
    }

    @Test
    public void getAllActiveSessionsByTheme() {
        List<Session> sessionsByTheme = sessionService.getAllActiveSessionsByTheme(1L);
        Assert.assertEquals(1, sessionsByTheme.size());
    }

    @Test
    public void getAllPlannedSessionsByTheme() {
        List<Session> sessionsByTheme = sessionService.getAllPlannedSessionsByTheme(1L);
        Assert.assertEquals(1, sessionsByTheme.size());
    }

    @Test
    public void getAllSessionsByOrganiser() {
        List<Session> sessionsByTheme = sessionService.getAllSessionsByOrganiser(1L);
        Assert.assertEquals(3, sessionsByTheme.size());
    }

    @Test
    public void getAllSessionsByUser() {
        List<Session> sessionsByTheme = sessionService.getAllSessionsByUser(2L);
        Assert.assertEquals(3, sessionsByTheme.size());
    }

    @Test
    public void getAllActiveSessionsOfUser() {
        List<Session> sessionsByTheme = sessionService.getAllActiveSessionsOfUser(2L);
        Assert.assertEquals(1, sessionsByTheme.size());
    }

    @Test
    public void getAllPlannedSessionsOfUser() {
        List<Session> sessionsByTheme = sessionService.getAllPlannedSessionsOfUser(2L);
        Assert.assertEquals(1, sessionsByTheme.size());
    }

    @Test
    public void getAllPassedSessionsOfUser() {
        List<Session> sessionsByTheme = sessionService.getAllPassedSessionsOfUser(2L);
        Assert.assertEquals(1, sessionsByTheme.size());
    }

    @Test
    public void testStartSession(){
        session = sessionService.startSession(session.getId());
        Assert.assertEquals(session.getOrganiser(), session.getActiveUser());
    }

    @Test
    public void testOneTurnSucces(){
        for (CardSession cardSession : cardSessions){
            cardSession.setSession(session);
            cardSessionService.save(cardSession);
        }
        session = sessionService.startSession(session.getId());
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user);
        Assert.assertNotEquals(session.getOrganiser(), session.getActiveUser());
        Assert.assertEquals(1, cardSessions.get(0).getPriority());
    }

    @Test(expected = CustomException.class)
    public void testOneTurnFailed(){
        for (CardSession cardSession : cardSessions){
            cardSession.setSession(session);
            cardSessionService.save(cardSession);
        }
        session = sessionService.startSession(session.getId());
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user2);
    }

    @Test
    public void testTakeTurnsSucces(){
        for (CardSession cardSession : cardSessions){
            cardSession.setSession(session);
            cardSessionService.save(cardSession);
        }
        session = sessionService.startSession(session.getId());
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user);
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user2);
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user);
        Assert.assertTrue(session.isGameOver());
    }

    @Test
    public void testTakeTurnsMaxPriority(){
        for (CardSession cardSession : cardSessions){
            cardSession.setSession(session);
            cardSessionService.save(cardSession);
        }
        session = sessionService.startSession(session.getId());
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user);
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user2);
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user);
        Assert.assertTrue(session.isGameOver());
        Assert.assertEquals("Max priority reached", session.getNotificationMessage());
    }

    @Test
    public void testTakeTurnsMaxNrOfRounds(){
        for (CardSession cardSession : cardSessions){
            cardSession.setSession(session);
            cardSessionService.save(cardSession);
        }
        session = sessionService.startSession(session.getId());
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user);
        session = sessionService.takeTurnWithChecks(cardSessions.get(1).getId(), session, user2);
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user);
        session = sessionService.takeTurnWithChecks(cardSessions.get(1).getId(), session, user2);
        Assert.assertTrue(session.isGameOver());
        Assert.assertEquals("Max number of rounds reached", session.getNotificationMessage());
    }

    @Test(expected = CustomException.class)
    public void testTakeIllegalTurnMaxNrOfRounds(){
        for (CardSession cardSession : cardSessions){
            cardSession.setSession(session);
            cardSessionService.save(cardSession);
        }
        session = sessionService.startSession(session.getId());
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user);
        session = sessionService.takeTurnWithChecks(cardSessions.get(1).getId(), session, user2);
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user);
        session = sessionService.takeTurnWithChecks(cardSessions.get(1).getId(), session, user2);
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user);
    }

    @Test(expected = CustomException.class)
    public void testTakeIllegalTurnMaxPriority(){
        for (CardSession cardSession : cardSessions){
            cardSession.setSession(session);
            cardSessionService.save(cardSession);
        }
        session = sessionService.startSession(session.getId());
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user);
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user2);
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user);
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user2);
    }

    @Test
    public void testEndSessionEarly(){
        for (CardSession cardSession : cardSessions){
            cardSession.setSession(session);
            cardSessionService.save(cardSession);
        }
        session = sessionService.startSession(session.getId());
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user);
        session = sessionService.takeTurnWithChecks(cardSessions.get(1).getId(), session, user2);
        session = sessionService.endSession(session.getId());
        Assert.assertTrue(session.isGameOver());
    }

    @Test(expected = CustomException.class)
    public void testTurnAfterEndSessionEarly(){
        for (CardSession cardSession : cardSessions){
            cardSession.setSession(session);
            cardSessionService.save(cardSession);
        }
        session = sessionService.startSession(session.getId());
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user);
        session = sessionService.takeTurnWithChecks(cardSessions.get(1).getId(), session, user2);
        session = sessionService.endSession(session.getId());
        session = sessionService.takeTurnWithChecks(cardSessions.get(0).getId(), session, user);
    }
}