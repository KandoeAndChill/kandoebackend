package be.kdg.kandoebackend.service;

import be.kdg.kandoebackend.error.CustomException;
import be.kdg.kandoebackend.model.Card;
import be.kdg.kandoebackend.model.Rating;
import be.kdg.kandoebackend.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import javax.transaction.Transactional;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Transactional
public class RatingServiceTest {

    @Autowired
    UserService userService;
    @Autowired
    CardService cardService;

    private User user;
    private User user2;
    private Card card;
    private Rating rating;
    private Rating rating2;

    @Before
    public void setupTests() {
        user = new User();
        user.setUsername("FirstUser");
        user.setPassword("password123");
        user.setEmail("test@123.be");
        userService.save(user);

        user2 = new User();
        user2.setUsername("FirstUser2");
        user2.setPassword("password123");
        user2.setEmail("test@124.be");
        userService.save(user2);

        card = new Card();
        card.setName("test1");
        card.setImageBase64Url("link");
        card = cardService.save(card);

        rating = new Rating();
        rating.setComment("testtest");

        rating2 = new Rating();
        rating2.setComment("testtest2");
    }

    @Test
    public void testAddCardRating() {
        List<Rating> ratingList = cardService.addRating(card.getId(), rating, user);
        Assert.assertEquals(1, ratingList.size());
        Assert.assertEquals("testtest", ratingList.get(0).getComment());
        Assert.assertEquals(user.getUsername(), ratingList.get(0).getUser().getUsername());
    }

    @Test
    public void getRatingByCard() {
        List<Rating> ratingList;
        cardService.addRating(card.getId(), rating, user);
        ratingList = cardService.addRating(card.getId(), rating2, user2);
        Assert.assertEquals(2, ratingList.size());
    }

    @Test(expected = CustomException.class)
    public void rateCardTwice() {
        List<Rating> ratingList;
        cardService.addRating(card.getId(), rating, user);
        ratingList = cardService.addRating(card.getId(), rating2, user);
    }
}