package be.kdg.kandoebackend.service;

import be.kdg.kandoebackend.error.CustomException;
import be.kdg.kandoebackend.model.Card;
import be.kdg.kandoebackend.model.Tag;
import be.kdg.kandoebackend.model.Theme;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class CardServiceTest {

    @Autowired
    private CardService cardService;
    @Autowired
    private ThemeService themeService;

    private Theme theme;
    private Card card;


    @Before
    public void setupTests(){
        List<Tag> tagList = new ArrayList<>();
        theme = new Theme();
        theme.setName("testTheme");
        theme.setDescription("testtesttesttheme");
        theme.setTagList(tagList);
        themeService.save(theme);

        card = new Card();
        card.setId(1L);
        card.setName("Cardname");
        card.setImageBase64Url("link");
        card.setTheme(theme);
    }

    @Test
    public void testCardService() throws CustomException {
        Card savedCard = cardService.save(card);
        Card loadedCard = cardService.load(1L);
        Assert.assertEquals(savedCard, loadedCard);
        Assert.assertEquals("Name should be the same", card.getName(), savedCard.getName());
        Assert.assertEquals("Image should be the same", card.getImageBase64Url(), savedCard.getImageBase64Url());
    }

    @Test
    public void testGetAllCardsForTheme() throws CustomException {
        Card card2 = new Card();
        card2.setId(2L);
        card2.setName("Cardname2");
        card2.setImageBase64Url("link2");
        card2.setTheme(theme);
        cardService.save(card);
        cardService.save(card2);

        Assert.assertEquals(2, cardService.getCardsForTheme(theme.getId()).size());
        Assert.assertEquals("Name should match", card.getName(), cardService.getCardsForTheme(1L).get(0).getName());
        Assert.assertEquals("Name should match", card2.getName(), cardService.getCardsForTheme(1L).get(1).getName());
    }
}