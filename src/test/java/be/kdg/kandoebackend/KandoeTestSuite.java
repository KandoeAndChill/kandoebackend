package be.kdg.kandoebackend;

import be.kdg.kandoebackend.controller.rest.CardRestControllerTest;
import be.kdg.kandoebackend.controller.rest.SessionRestControllerTest;
import be.kdg.kandoebackend.service.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(Suite.class)
@SpringBootTest
@SuiteClasses({CardRestControllerTest.class, SessionRestControllerTest.class, CardServiceTest.class,
                     CardSessionServiceTest.class, SessionServiceTest.class, ThemeServiceTest.class, UserServiceTest.class, RatingServiceTest.class})
public class KandoeTestSuite {
}
