package be.kdg.kandoebackend.acceptance;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class E2EUser {
    private static WebDriver driver;
    private final String URL = "http://localhost:4200";

    @BeforeClass
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", ".\\src\\test\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @AfterClass
    public static void tearDownClass() {
        driver.close();
    }

    @Test
    public void registerUser(){
        WebElement button;

        driver.get(URL + "/register");

        button = driver.findElement(By.id("register"));

        driver.findElement(By.id("email")).sendKeys("value", "string@123.test");
        driver.findElement(By.id("username")).sendKeys("value", "FirstUser");
        driver.findElement(By.id("password")).sendKeys("value", "string123456");
        button.click();
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.urlContains("login"));
    }

    @Test
    public void loginUser(){
        WebElement button;

        driver.get(URL + "/login");
        driver.findElement(By.id("username")).sendKeys("value", "FirstUser");
        driver.findElement(By.id("password")).sendKeys("value", "string123456");
        button = driver.findElement(By.id("login"));
        button.click();
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.urlToBe(URL + "/"));
    }
}
