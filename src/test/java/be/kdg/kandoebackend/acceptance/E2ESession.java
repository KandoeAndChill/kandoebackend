package be.kdg.kandoebackend.acceptance;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

/**
 * IMPORTANT:   increment sessionId by 1 with each run
 *              increment themeId by 1 after running 1 test and by 2 after running 2
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class E2ESession {
    private static WebDriver driver;
    private final static String URL = "http://localhost:4200";
    private static int themeId = 25;
    private static int sessionId = 17;

    @BeforeClass
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", ".\\src\\test\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

        //login
        WebElement button;
        driver.get(URL + "/login");
        driver.findElement(By.id("username")).sendKeys("value", "FirstUser");
        driver.findElement(By.id("password")).sendKeys("value", "string123456");
        button = driver.findElement(By.id("login"));
        button.click();
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.urlToBe(URL + "/"));
        WebElement button2;
        WebElement button3;

        // create theme
        driver.get(URL + "/themes");
        button2 = driver.findElement(By.id("themeCreate"));
        button2.click();
        new WebDriverWait(driver, 5);
        driver.findElement(By.id("name")).sendKeys("FirstTheme");
        driver.findElement(By.id("description")).sendKeys("Description of FirstTheme");
        button3 = driver.findElement(By.id("postTheme"));
        button3.click();
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.urlToBe(URL + "/themes"));

        // Navigate to Theme
        driver.get(URL + "/theme/" + themeId);

        // Create card
        WebElement cardButton = driver.findElement(By.id("cardcreatebutton"));
        cardButton.click();
        driver.findElement(By.id("name")).sendKeys("TheBestCard");
        WebElement submitButton = driver.findElement(By.id("submitcard"));
        submitButton.click();
        new WebDriverWait(driver, 5);
        driver.findElement(By.id("navigateSession")).click();
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.urlContains("/theme/" + themeId +"/sessions"));

    }

    @Test
    public void createSession(){
        driver.get(URL + ("/sessions"));
        WebElement button;
        button = driver.findElement(By.id("sessionCreate"));
        button.click();

        driver.findElement(By.id("name")).sendKeys("FirstSession");
        driver.findElement(By.id("theme")).click();
        driver.findElement(By.id("mat-option-17")).click();

        WebElement submitButton = driver.findElement(By.id("submitsession"));
        submitButton.click();
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.urlToBe(URL + "/sessions"));
    }

    @Test
    public void playSession(){
        //sessie aanmaken
        driver.findElement(By.id("cardcreatebutton3")).click();
        driver.findElement(By.id("name")).sendKeys("FirstSession");
        driver.findElement(By.id("toggleusercards")).click();
        WebElement submitButton = driver.findElement(By.id("submitsession"));
        submitButton.click();
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.urlToBe(URL + "/theme/" + themeId + "/sessions"));

        //Go to session and get redirected
        driver.get(URL + "/session/" + sessionId);
        new WebDriverWait(driver, 10).until(ExpectedConditions.urlContains("/select/"));

        //Select and add first card
        driver.findElement(By.className("cardpreview")).click();
        new WebDriverWait(driver, 5);
        driver.findElement(By.id("submitcardsbutton")).click();
        new WebDriverWait(driver, 10);

        //Start phase 2
        driver.findElement(By.id("startsessionbutton")).click();
        new WebDriverWait(driver, 10).until(ExpectedConditions.urlContains("/session"));

        //Select first card 5 times until statustext is visible
        for(int i = 0; i<5; i++) {
            driver.findElement(By.id("cardpreview")).click();
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOf(driver.findElement(By.className("statustext"))));
    }
}