package be.kdg.kandoebackend.acceptance;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class E2ETheme {
    private static WebDriver driver;
    private final static String URL = "http://localhost:4200";

    @BeforeClass
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", ".\\src\\test\\resources\\chromedriver.exe");
        driver = new ChromeDriver();

        WebElement button;

        driver.get(URL + "/login");
        driver.findElement(By.id("username")).sendKeys("value", "FirstUser");
        driver.findElement(By.id("password")).sendKeys("value", "string123456");
        button = driver.findElement(By.id("login"));
        button.click();
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.urlToBe(URL + "/"));
    }

    @AfterClass
    public static void tearDownClass() {
        driver.close();
    }

    @Test
    public void testCreateTheme(){
        WebElement button;
        WebElement button2;

        driver.get(URL + "/themes");
        button = driver.findElement(By.id("themeCreate"));
        button.click();

        driver.findElement(By.id("name")).sendKeys("FirstTheme");
        driver.findElement(By.id("description")).sendKeys("Description of FirstTheme");
        button2 = driver.findElement(By.id("postTheme"));
        button2.click();
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.urlToBe(URL + "/themes"));
    }

    @Test
    public void deleteTheme(){
        driver.get(URL + "/themes");
        driver.navigate().refresh();
        int themeCount = driver.findElements(By.id("themeName")).size();
        WebElement deleteButton = driver.findElement(By.id("deletebutton"));
        deleteButton.click();
        driver.navigate().refresh();
        int themeCount2 = driver.findElements(By.id("themeName")).size();
        assertEquals(themeCount-themeCount2, 1);
    }
}
