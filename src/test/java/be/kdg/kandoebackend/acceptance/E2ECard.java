package be.kdg.kandoebackend.acceptance;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class E2ECard {
    private static WebDriver driver;
    private final static String URL = "http://localhost:4200";

    @BeforeClass
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", ".\\src\\test\\resources\\chromedriver.exe");
        driver = new ChromeDriver();

        WebElement button;

        driver.get(URL + "/login");
        driver.findElement(By.id("username")).sendKeys("value", "FirstUser");
        driver.findElement(By.id("password")).sendKeys("value", "string123456");
        button = driver.findElement(By.id("login"));
        button.click();
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.urlToBe(URL + "/"));
    }

    @Test
    public void testCreateCard() {
        driver.get(URL + "/themes");

        // Navigate to Theme
        WebElement theme = driver.findElement(By.id("themeName"));
        theme.click();
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.urlContains("/theme/"));

        // Create card
        WebElement cardButton = driver.findElement(By.id("cardcreatebutton"));
        cardButton.click();

        driver.findElement(By.id("name")).sendKeys("TheBestCard");
        WebElement submitButton = driver.findElement(By.id("submitcard"));
        submitButton.click();
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.urlContains("/theme/"));

        WebElement successDiv;
        successDiv = driver.findElement(By.id("cardCreated"));
        assertEquals("Card Created!", successDiv.getText());
    }

    @Test
    public void testFalseCard() {
        driver.get(URL + "/themes");

        // Navigate to Theme
        WebElement theme = driver.findElement(By.id("themeName"));
        theme.click();
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.urlContains("/theme/"));

        WebElement cardButton = driver.findElement(By.id("cardcreatebutton"));
        cardButton.click();

        driver.findElement(By.id("name")).sendKeys("Bad");
        WebElement submitButton = driver.findElement(By.id("submitcard"));
        submitButton.click();

        WebElement failDiv;
        failDiv = driver.findElement(By.id("nameSize"));
        assertTrue(failDiv.isDisplayed());
    }
}