package be.kdg.kandoebackend.controller.rest;

import be.kdg.kandoebackend.dto.SessionDTO;
import be.kdg.kandoebackend.model.*;
import be.kdg.kandoebackend.service.CardService;
import be.kdg.kandoebackend.service.SessionService;
import be.kdg.kandoebackend.service.ThemeService;
import be.kdg.kandoebackend.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SessionRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CardService cardService;

    @Autowired
    private ThemeService themeService;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private UserService userService;

    private String loggedInUser;
    private SessionDTO sessionDTO;

    @Before
    public void setupSession() {
        User user = new User();
        user.setId(1L);
        user.setUsername("FirstUser");
        user.setPassword("password123");
        user.setEmail("test@123.be");
        userService.save(user);
        loggedInUser = userService.signin("FirstUser", "password123");

        User user2 = new User();
        user2.setId(2L);
        user2.setUsername("SecondUser");
        user2.setPassword("password123");
        user2.setEmail("test@124.be");
        userService.save(user2);

        User user3 = new User();
        user3.setId(3L);
        user3.setUsername("ThirdUser");
        user3.setPassword("password123");
        user3.setEmail("test@125.be");
        userService.save(user3);

        List<Tag> tagList = new ArrayList<>();
        Theme theme = new Theme();
        theme.setName("testTheme");
        theme.setDescription("testtesttesttheme");
        theme.setTagList(tagList);
        theme.setUser(user);
        theme.setCommentaryAllowed(false);
        themeService.save(theme);

        Card card = new Card();
        card.setTheme(theme);
        card.setImageBase64Url("link");
        card.setName("testcard");
        cardService.save(card);

        Session session = new Session();
        session.setName("testload");
        session.setChanceCircle(true);
        session.setTheme(theme);
        session.setMaxCards(3);
        session.setMinCards(1);
        session.setNumberOfCircles(5);
        session.setNumberOfRounds(5);
        session.setRoundTimeInMinutes(1);
        session.setStartDate(LocalDateTime.now());
        session.setOrganiser(user);
        sessionService.save(session);

        Session session2 = new Session();
        session2.setName("testload2");
        session2.setChanceCircle(true);
        session2.setTheme(theme);
        session2.setMaxCards(3);
        session2.setMinCards(1);
        session2.setNumberOfCircles(5);
        session2.setNumberOfRounds(5);
        session2.setRoundTimeInMinutes(1);
        session2.setStartDate(LocalDateTime.of(2014, Month.JANUARY, 1, 10, 10, 30));
        session2.setOrganiser(user);
        sessionService.save(session2);

        Session session3 = new Session();
        session3.setName("testload3");
        session3.setChanceCircle(true);
        session3.setTheme(theme);
        session3.setMaxCards(3);
        session3.setMinCards(1);
        session3.setNumberOfCircles(5);
        session3.setNumberOfRounds(5);
        session3.setRoundTimeInMinutes(1);
        session3.setStartDate(LocalDateTime.of(2420, Month.JANUARY, 1, 13, 37, 42));
        session3.setOrganiser(user);
        sessionService.save(session3);
    }

    @Test
    public void createSession() throws Exception{
        SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setName("test");
        sessionDTO.setChanceCircle(true);
        sessionDTO.setThemeId(1L);
        sessionDTO.setOrganiserId(1L);
        sessionDTO.setMaxCards(3);
        sessionDTO.setMinCards(1);
        sessionDTO.setNumberOfCircles(5);
        sessionDTO.setNumberOfRounds(5);
        sessionDTO.setRoundTimeInMinutes(1);
        sessionDTO.setStartTime(LocalDateTime.now());
        String requestJson = objectMapper.writeValueAsString(sessionDTO);

        mockMvc.perform(post("/sessions/save")
                .header("Authorization", "Bearer " + loggedInUser)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andExpect(status().isCreated())
                .andDo(print())
                .andExpect(jsonPath("$.name").value("test"));
    }

    @Test
    public void deleteSession() throws Exception{
    }

    @Test
    public void inviteToSession() throws Exception{

    }

    @Test
    public void loadSession() throws Exception{
        mockMvc.perform(get("/sessions/find/1/").header("Authorization", "Bearer " + loggedInUser).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("testload"));
    }

    @Test
    public void cloneSession() throws Exception{
    }

    @Test
    public void endSession() throws Exception{
    }

    @Test
    public void startSession() throws Exception{
    }

    @Test
    public void takeTurn() throws Exception{

    }

    @Test
    public void changeUserTurn() throws Exception{
    }

    @Test
    public void selectCardsForSession() throws Exception{
    }

    @Test
    public void createCardsForSession() throws Exception{
    }
}
