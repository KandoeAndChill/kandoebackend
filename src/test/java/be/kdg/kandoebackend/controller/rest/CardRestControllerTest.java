package be.kdg.kandoebackend.controller.rest;

import be.kdg.kandoebackend.dto.CardDTO;
import be.kdg.kandoebackend.model.Card;
import be.kdg.kandoebackend.model.Tag;
import be.kdg.kandoebackend.model.Theme;
import be.kdg.kandoebackend.service.CardService;
import be.kdg.kandoebackend.service.ThemeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CardRestControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CardService cardService;

    @Autowired
    private ThemeService themeService;

    @Before
    public void setupTheme() {
        List<Tag> tagList = new ArrayList<>();
        Theme theme = new Theme("Name", "jzenflflkjfze", tagList);
        themeService.save(theme);
    }

    @Test
    public void testCreateCard() throws Exception{
        CardDTO cardDTO = new CardDTO();
        cardDTO.setName("testtesttest");
        cardDTO.setImageBase64Url("link");
        cardDTO.setThemeId(1L);
        String requestJson = objectMapper.writeValueAsString(cardDTO);

        mockMvc.perform(post("/cards/save").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andExpect(status().isCreated())
                .andDo(print())
                .andExpect(jsonPath("$.name", is(cardDTO.getName())));
    }

    @Test
    public void testGetCard() throws Exception{
        Long id = 1L;
        Card card = new Card();
        card.setId(id);
        card.setName("testload");
        card.setImageBase64Url("link2");

        Card savedCard = cardService.save(card);

        mockMvc.perform(get("/cards/find/1/").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("testload"));
    }

    @Test
    public void testCreateCardWithNameTooShort() throws Exception {
        CardDTO cardDTO = new CardDTO();
        cardDTO.setName("bla");
        cardDTO.setImageBase64Url("link");
        cardDTO.setThemeId(1L);
        String requestJson = objectMapper.writeValueAsString(cardDTO);

        mockMvc.perform(post("/cards/save").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }

    @Test
    public void testCreateCardWithNameTooLong() throws Exception {
        CardDTO cardDTO = new CardDTO();
        cardDTO.setName("ThisNameisTooLongForOurTestBecauseItHasMoreThan20Characters");
        cardDTO.setImageBase64Url("link");
        cardDTO.setThemeId(1L);
        String requestJson = objectMapper.writeValueAsString(cardDTO);

        mockMvc.perform(post("/cards/save").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andExpect(status().isBadRequest())
                .andDo(print());
    }
}
