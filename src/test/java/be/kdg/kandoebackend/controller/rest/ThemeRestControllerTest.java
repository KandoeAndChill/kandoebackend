package be.kdg.kandoebackend.controller.rest;

import be.kdg.kandoebackend.dto.ThemeDTO;
import be.kdg.kandoebackend.model.Tag;
import be.kdg.kandoebackend.model.Theme;
import be.kdg.kandoebackend.model.User;
import be.kdg.kandoebackend.service.ThemeService;
import be.kdg.kandoebackend.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ThemeRestControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private ThemeService themeService;
    @Autowired
    private UserService userService;

    private User user;

    @Before
    public void setup(){
        user = new User();
        user.setId(1L);
        user.setUsername("FirstUser");
        user.setPassword("password123");
        user.setEmail("test@123.be");
        userService.save(user);
    }

    @Test
    public void createTheme() throws Exception{
        String loggedInUser = userService.signin("FirstUser", "password123");

        List<Tag> tagList = new ArrayList<>();
        ThemeDTO themeDTO = new ThemeDTO();
        themeDTO.setName("TestName");
        themeDTO.setDescription("TestDescription");
        themeDTO.setUserId(1L);
        themeDTO.setTagList(tagList);
        String requestJson = objectMapper.writeValueAsString(themeDTO);

        mockMvc.perform(post("/themes/save").header("Authorization", "Bearer " + loggedInUser).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(requestJson))
                .andExpect(status().isCreated())
                .andDo(print())
                .andExpect(jsonPath("$.name", is(themeDTO.getName())));
    }

    @Test
    public void loadTheme() throws Exception{
        List<Tag> tagList = new ArrayList<>();
        Theme theme = new Theme();
        theme.setName("TestName");
        theme.setDescription("TestDescription");
        theme.setUser(user);
        theme.setTagList(tagList);
        themeService.save(theme);

        mockMvc.perform(get("/themes/find/1/").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("TestName"));
    }

    @Test
    public void deleteTheme() throws Exception{
        List<Tag> tagList = new ArrayList<>();
        Theme theme = new Theme();
        theme.setName("TestName");
        theme.setDescription("TestDescription");
        theme.setUser(user);
        theme.setTagList(tagList);
        themeService.save(theme);

        mockMvc.perform(delete("/themes/delete/1/").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void testToBeDeletedThemeNotPresent() throws Exception{
        mockMvc.perform(delete("/themes/delete/1/").contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print()).andExpect(status().isNotFound());
    }

    @Test
    public void getAllThemesOfUser() throws Exception{
        String loggedInUser = userService.signin("FirstUser", "password123");

        List<Tag> tagList = new ArrayList<>();
        Theme theme = new Theme();
        theme.setName("TestName");
        theme.setDescription("TestDescription");
        theme.setUser(user);
        theme.setTagList(tagList);
        Theme theme2 = new Theme();
        theme2.setName("TestName2");
        theme2.setDescription("TestDescription2");
        theme2.setUser(user);
        theme2.setTagList(tagList);
        themeService.save(theme);
        themeService.save(theme2);

        mockMvc.perform(get("/themes/findAllByLoggedInUser").header("Authorization", "Bearer " + loggedInUser).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(jsonPath("$", hasSize(2)));
    }
}
